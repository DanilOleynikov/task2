package com.epam.newsmanagement.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml" }, type = DatabaseOperation.DELETE)
public class HibernateAuthorDaoImplTest {

	@Autowired
	private AuthorDao authorDao;

	@Test
	public void testUpdateAuthorName() throws DaoException {

		Author author = new Author();
		Long authorIdToUpdate = new Long(1);
		String updatedName = "TestName";

		authorDao.updateAuthorName(authorIdToUpdate, updatedName);
		author = authorDao.loadById(authorIdToUpdate);
		Assert.assertEquals(author.getName(), updatedName);
	}

	@Test
	public void testAdd() throws DaoException, ParseException {
		Author expectedAuthor = new Author();
		expectedAuthor.setExpiredTime(new Date());
		expectedAuthor.setName("TestName");
		Long authorId = authorDao.add(expectedAuthor);
		Author actualAuthor = new Author();
		actualAuthor = authorDao.loadById(authorId);
		assertAuthor(expectedAuthor, actualAuthor);
	}

	@Test
	public void testUpdate() throws DaoException, ParseException {

		Author author = new Author();
		author.setExpiredTime(new Date());
		author.setName("UpdatedName");
		author.setId(new Long(1));
		authorDao.update(author);

		Author actualAuthor = new Author();
		actualAuthor = authorDao.loadById(new Long(1));
		assertAuthor(author, actualAuthor);
	}

	@Test
	public void testLoadById() throws DaoException {

		Author author = new Author();
		String expectedDate = "2015-08-15";
		author = authorDao.loadById(new Long(1));
		Assert.assertEquals(author.getName(), "Danil");
		Assert.assertEquals(expectedDate, author.getExpiredTime().toString());
	}

	@Test
	public void testLoadAll() throws DaoException {
		List<Author> authors = new ArrayList<Author>();
		authors = authorDao.loadAll();
		Assert.assertEquals(3, authors.size());
	}

	@Test
	public void testLoadAllNonExpired() throws DaoException {

		List<Author> nonExpiredAuthors = new ArrayList<Author>();
		nonExpiredAuthors = authorDao.loadAllNonExpired();
		Assert.assertEquals(1, nonExpiredAuthors.size());
	}

	@Test
	public void updateAuthorExpired() throws DaoException, ParseException {
		Author authorAfterExpiriring = new Author();
		Date dateNow = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date formattedDateNow = formatter.parse(formatter.format(dateNow));

		authorDao.updateAuthorExpired(new Long(1));
		authorAfterExpiriring = authorDao.loadById(new Long(1));

		Assert.assertEquals(formattedDateNow.getTime(), authorAfterExpiriring
				.getExpiredTime().getTime());
	}

	private void assertAuthor(Author expected, Author actual)
			throws ParseException {

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date formatedExpectedAuthorExpDate = formatter.parse(formatter
				.format(expected.getExpiredTime()));

		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(formatedExpectedAuthorExpDate.getTime(), actual
				.getExpiredTime().getTime());
		Assert.assertEquals(expected.getId(), actual.getId());
	}
}
