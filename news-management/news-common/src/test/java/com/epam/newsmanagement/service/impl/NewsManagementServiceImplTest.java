package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * This class is testing
 * {@link com.epam.newsmanagement.service.impl.NewsManagementServiceImpl} class.
 * Here we are using Mockito framework and mocked objects. In our case we have
 * mocked objects of
 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl} and
 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl}, that then set
 * to real object of
 * {@link com.epam.newsmanagement.service.impl.NewsManagementServiceImpl}; NOTE:
 * all methods that are testing here are not calling during tests, they are
 * calling on mocked objects, so there are NO interactions with database during
 * test invocation. So we can only check if necessary method was called and what
 * value does it return.
 *
 */

public class NewsManagementServiceImplTest {

	private CommentServiceImpl commentService;
	private NewsServiceImpl newsService;

	private NewsManagementServiceImpl newsManagementService;

	/**
	 * This method is executing before test methods. It create object of
	 * {@link com.epam.newsmanagement.service.impl.NewsManagementServiceImpl}
	 * class, also as mocked object of
	 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl} and
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl} classes;
	 * 
	 */

	@Before
	public void setUp() {
		commentService = Mockito.mock(CommentServiceImpl.class);
		newsService = Mockito.mock(NewsServiceImpl.class);

		newsManagementService = new NewsManagementServiceImpl();

		newsManagementService.setCommentService(commentService);
		newsManagementService.setNewsService(newsService);

	}

	/**
	 * This method is testing is the necessary methods( in our case
	 * 
	 * <pre>
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#add(News)}
	 * 
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#addNewsAuthorLink(Long, Long)}
	 * 
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#addNewsTagLink(Long, Long...)}
	 * 
	 * invoked) and what values does it return.
	 */
	@Test
	public void testSaveNewsWithAuthorAndTags() throws ServiceException {

		News dummyNews = new News();
		Long expectedNewsId = new Long(1);
		dummyNews.setId(expectedNewsId);
		Author dummyAuthor = new Author();
		Long dummyAuthorId = new Long(1);
		dummyAuthor.setId(dummyAuthorId);

		Long firstDummyTagId = new Long(1);
		Long secondDummyTagId = new Long(2);

		when(newsService.add(dummyNews)).thenReturn(expectedNewsId);
		Long actualNewsId = newsManagementService.saveNewsWithAuthorAndTags(
				dummyNews, dummyAuthorId, firstDummyTagId, secondDummyTagId);
		Assert.assertEquals(expectedNewsId, actualNewsId);

		verify(newsService, atLeastOnce()).add(dummyNews);
		verify(newsService, atLeastOnce()).addNewsAuthorLink(expectedNewsId,
				dummyAuthorId);
		verify(newsService, atLeastOnce()).addNewsTagLink(expectedNewsId,
				firstDummyTagId, secondDummyTagId);

	}

	/**
	 * This method is testing is the necessary methods( in our case
	 * 
	 * <pre>
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#deleteNewsAuthorLinkByNewsId(Long)}
	 * 
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#deleteNewsTagLinkByNewsId(Long)}
	 * 
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl#delete(Long)}
	 * 
	 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl#deleteByNewsId(Long)}
	 * 
	 * invoked).
	 */
	@Test
	public void testDeleteNews() throws ServiceException {

		Long dummyNewsId = new Long(1);
		newsManagementService.deleteNews(dummyNewsId);

		verify(newsService, atLeastOnce()).deleteNewsAuthorLinkByNewsId(
				dummyNewsId);
		verify(commentService, atLeastOnce()).deleteByNewsId(dummyNewsId);
		verify(newsService, atLeastOnce()).deleteNewsTagLinkByNewsId(
				dummyNewsId);
		verify(newsService, atLeastOnce()).delete(dummyNewsId);

	}
}
