package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * This class is testing
 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl} class. Here we
 * are using Mockito framework and mocked objects. In our case we have mocked
 * object of {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl} that then
 * set to real object of
 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl}; NOTE: all
 * methods that are testing here are not calling during tests, they are calling
 * on mocked objects, so there are NO interactions with database during test
 * invocation. So we can only check if necessary method was called and what
 * value does it return.
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	private JdbcNewsDaoImpl newsDao;
	private NewsServiceImpl newsService;

	/**
	 * This method is executing before test methods. It create object of
	 * {@link com.epam.newsmanagement.service.impl.NewsServiceImpl} class, also
	 * as mocked object of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl} class.
	 * 
	 */

	@Before
	public void setUp() {
		newsDao = Mockito.mock(JdbcNewsDaoImpl.class);
		newsService = new NewsServiceImpl();
		newsService.setNewsDao(newsDao);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#add(News)}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testAdd() throws ServiceException, DaoException {
		News dummyNews = new News();
		Long dummyId = new Long(1);
		when(newsService.add(dummyNews)).thenReturn(dummyId);

		Long actual = newsService.add(dummyNews);
		Assert.assertEquals(dummyId, actual);
		verify(newsDao, atLeastOnce()).add(dummyNews);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#update(News)}
	 * invoked).
	 */

	@Test
	public void testUpdate() throws ServiceException, DaoException {
		News dummyNews = new News();
		newsService.update(dummyNews);
		verify(newsDao, atLeastOnce()).update(dummyNews);
		;
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#loadById(Long)}
	 * invoked) and what value does it return.
	 */
	@Test
	public void testLoadById() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		News dummyNews = new News();
		News expected = new News();

		when(newsService.loadById(dummyId)).thenReturn(dummyNews);
		expected = newsService.loadById(dummyId);

		verify(newsDao, atLeastOnce()).loadById(dummyId);
		Assert.assertEquals(expected, dummyNews);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#delete(Long)}
	 * invoked.
	 */
	@Test
	public void testDelete() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		newsService.delete(dummyId);
		verify(newsDao, atLeastOnce()).delete(dummyId);
	}

	@Test
	public void testLoadNewsVo() throws ServiceException, DaoException {

		Long dummyNewsId = new Long(1);
		NewsVO dummyNewsVo = new NewsVO();
		NewsVO expectedNewsVo = new NewsVO();

		News dummyNews = new News();
		dummyNews.setCreationDate(new Date(new Date().getTime()));
		dummyNews.setFullText("dummy full text");
		dummyNews.setId(dummyNewsId);
		dummyNews.setModificationDate(new Date(new Date().getTime()));
		dummyNews.setShortText("dummy short text");
		dummyNews.setTitle("dummy short title");

		Tag dummyTag = new Tag();
		dummyTag.setId(new Long(1));
		dummyTag.setName("test dummy tag");

		Author dummyAuthor = new Author();
		dummyAuthor.setExpiredTime(new Date(new Date().getTime()));
		dummyAuthor.setId(new Long(1));
		dummyAuthor.setName("dummy author name");

		Comment dummyComment = new Comment();
		dummyComment.setCreationTime(new Date(new Date().getTime()));
		dummyComment.setId(new Long(1));
		dummyComment.setNewsId(dummyNewsId);
		dummyComment.setText("dummy comment text");

		Set<Tag> tags = new HashSet<Tag>();
		Set<Comment> comments = new HashSet<Comment>();

		tags.add(dummyTag);
		comments.add(dummyComment);
		dummyNewsVo.setAuthor(dummyAuthor);
		dummyNewsVo.setComments(comments);
		dummyNewsVo.setNews(dummyNews);
		dummyNewsVo.setTags(tags);

		when(newsService.loadNewsVo(dummyNewsId)).thenReturn(dummyNewsVo);
		expectedNewsVo = newsService.loadNewsVo(dummyNewsId);
		verify(newsDao, atLeastOnce()).loadNewsVo(dummyNewsId);
		Assert.assertEquals(dummyNewsVo, expectedNewsVo);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#loadAll()}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testLoadAll() throws ServiceException, DaoException {
		List<News> dummyListOfNews = new ArrayList<News>();
		List<News> actualListOfNews = new ArrayList<News>();

		when(newsService.loadAll()).thenReturn(dummyListOfNews);
		actualListOfNews = newsService.loadAll();

		verify(newsDao, atLeastOnce()).loadAll();
		Assert.assertEquals(dummyListOfNews, actualListOfNews);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#findByCriteria(SearchCriteria)}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testFindByCriteria() throws ServiceException, DaoException {

		SearchCriteria dummySearchCriteria = new SearchCriteria();
		dummySearchCriteria.setAuthorId(new Long(1));
		dummySearchCriteria.setTagId(new Long[] { new Long(1), new Long(2) });

		List<News> dummyListOfNews = new ArrayList<News>();
		List<News> actualListOfNews = new ArrayList<News>();

		when(newsService.findByCriteria(dummySearchCriteria)).thenReturn(
				dummyListOfNews);
		actualListOfNews = newsService.findByCriteria(dummySearchCriteria);
		Assert.assertEquals(dummyListOfNews, actualListOfNews);

	}
}
