package com.epam.newsmanagement.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml" }, type = DatabaseOperation.DELETE_ALL)
public class JpaNewsDaoImplTest {

	@Autowired
	private NewsDao newsDao;

	public void testLoadAllNewsId() throws DaoException {
		List<Long> newsId = new ArrayList<Long>();
		newsId = newsDao.loadAllNewsId();
		Assert.assertEquals(3, newsId.size());
	}

	@Test
	public void testLoadNewsIdSearchedByCriteria() throws DaoException {
		SearchCriteria sc = new SearchCriteria();
		Long authorId = new Long(1);
		Long[] tagId = { new Long(1) };
		sc.setAuthorId(authorId);
		sc.setTagId(tagId);

		List<Long> newsIdSerachedByCriteria = newsDao
				.loadNewsIdSearchedByCriteria(sc, new Long(0));
		Assert.assertEquals(1, newsIdSerachedByCriteria.size());
	}

	@Test
	public void testLoadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate()
			throws DaoException {

		List<Long> newsIdSortedByNumberOfCommentsAndModifDate = newsDao
				.loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(new Long(
						0));
		Assert.assertEquals(1, newsIdSortedByNumberOfCommentsAndModifDate
				.get(0).intValue());
		Assert.assertEquals(2, newsIdSortedByNumberOfCommentsAndModifDate
				.get(1).intValue());
		Assert.assertEquals(3, newsIdSortedByNumberOfCommentsAndModifDate
				.get(2).intValue());

	}

	@Test
	public void testLoadListOfNewsVoByNewsId() throws DaoException {
		List<Long> newsIdToLoad = new ArrayList<Long>() {
			{
				add(new Long(1));
				add(new Long(2));
			}
		};

		List<NewsVO> actualNewsVoList = new ArrayList<NewsVO>();
		actualNewsVoList = newsDao.loadListOfNewsVoByNewsId(newsIdToLoad);
		Assert.assertEquals(2, actualNewsVoList.size());
	}

	@Test
	public void testGetNewsCount() throws DaoException {
		Long newsCount = newsDao.getNewsCount();
		Assert.assertEquals(new Long(3), newsCount);

	}

	@Test
	public void testAdd() throws DaoException {
		News news = new News();
		news = buildNews();
		Long newsId = newsDao.add(news);
		News actualNews = new News();
		actualNews = newsDao.loadById(newsId);
		Assert.assertNotNull(actualNews);
	}

	@Test
	public void testUpdate() throws DaoException {
		News news = new News();
		news = buildNews();
		news.setId(new Long(1));
		newsDao.update(news);

		News actualNews = new News();
		actualNews = newsDao.loadById(news.getId());
		Assert.assertEquals(news, actualNews);

	}

	@Test
	public void testDelete() throws DaoException {

		News news = new News();
		news = buildNews();
		Long newsId = newsDao.add(news);
		List<News> newsListBeforeDeleting = newsDao.loadAll();
		newsDao.delete(newsId);
		List<News> newsListAfterDeleting = newsDao.loadAll();
		Assert.assertTrue(newsListBeforeDeleting.size() > newsListAfterDeleting
				.size());
	}

	@Test
	public void testLoadById() throws DaoException {
		News news = new News();
		news = newsDao.loadById(new Long(1));
		Assert.assertNotNull(news.getFullText());
		Assert.assertNotNull(news.getShortText());
		Assert.assertNotNull(news.getTitle());
		Assert.assertNotNull(news.getCreationDate());
		Assert.assertNotNull(news.getId());
		Assert.assertNotNull(news.getModificationDate());
	}

	@Test
	public void testLoadNewsVo() throws DaoException {
		NewsVO newsVo = new NewsVO();
		newsVo = newsDao.loadNewsVo(new Long(1));
		Assert.assertNotNull(newsVo.getAuthor());
		Assert.assertNotNull(newsVo.getComments());
		Assert.assertNotNull(newsVo.getNews());
		Assert.assertNotNull(newsVo.getTags());
	}

	@Test
	public void testLoadAll() throws DaoException {
		List<News> allNews = newsDao.loadAll();
		Assert.assertEquals(3, allNews.size());
	}

	private News buildNews() {
		News news = new News();
		news.setCreationDate(new Timestamp(new Date().getTime()));
		news.setFullText("test full text");
		news.setModificationDate(new Date(new Date().getTime()));
		news.setShortText("test short text");
		news.setTitle("test title");
		return news;
	}
}
