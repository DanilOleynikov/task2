package com.epam.newsmanagement.dao.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml" }, type = DatabaseOperation.DELETE_ALL)
public class HibernateNewsDaoImplTest {

	@Autowired
	private NewsDao newsDao;

	// @Test
	public void testLoadAllNewsId() throws DaoException {
		int newsCount = newsDao.loadAll().size();
		List<Long> newsIdList = new ArrayList<Long>();
		newsIdList = newsDao.loadAllNewsId();
		Assert.assertEquals(newsCount, newsIdList.size());

	}

	// @Test
	public void testLoadNewsIdSearchedByCriteria() throws DaoException {

		List<Long> newsIdListSearchedByCriteria = new ArrayList<Long>();
		int numberOfExpectedSearchedNews = 1;

		SearchCriteria sc = new SearchCriteria();
		Long[] tagId = { new Long(1) };
		sc.setAuthorId(new Long(1));
		sc.setTagId(tagId);
		newsIdListSearchedByCriteria = newsDao.loadNewsIdSearchedByCriteria(sc,
				new Long(0));
		Assert.assertEquals(numberOfExpectedSearchedNews,
				newsIdListSearchedByCriteria.size());
	}

	// @Test
	public void testLoadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate()
			throws DaoException {
		List<Long> sortedNewsIdList = new ArrayList<Long>();
		sortedNewsIdList = newsDao
				.loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(new Long(
						0));
		Assert.assertEquals(1, sortedNewsIdList.get(0).intValue());
		Assert.assertEquals(3, sortedNewsIdList.get(2).intValue());

	}

	// @Test
	public void testLoadListOfNewsVoByNewsId() throws DaoException {
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		List<Long> newsId = new ArrayList<Long>() {
			{
				add(new Long(1));
				add(new Long(2));
			}
		};
		newsVoList = newsDao.loadListOfNewsVoByNewsId(newsId);
		Assert.assertEquals(2, newsVoList.size());

	}

	// @Test
	public void testGetNewsCount() throws DaoException {
		Long newsCount = new Long(0);
		newsCount = newsDao.getNewsCount();
		Assert.assertEquals(newsCount, new Long(3));
	}

	// @Test
	public void testAdd() throws DaoException {
		Long newsCountBeforeAdding = newsDao.getNewsCount();
		News news = new News();
		news = buildNews();
		newsDao.add(news);
		Long newsCountAfterAdding = newsDao.getNewsCount();
		Assert.assertTrue(newsCountBeforeAdding < newsCountAfterAdding);

	}

	// @Test
	public void testUpdate() throws DaoException, ParseException {

		News news = new News();
		news = buildNews();
		newsDao.update(news);
		News updatedNews = newsDao.loadById(news.getId());
		assertNews(news, updatedNews);

	}

	// @Test
	public void testDelete() throws DaoException {

		News news = new News();
		news = buildNews();
		Long newsId = newsDao.add(news);
		Long newsCountBeforeDeleting = newsDao.getNewsCount();
		newsDao.delete(newsId);
		Long newsCountAfterDeleting = newsDao.getNewsCount();
		Assert.assertTrue(newsCountBeforeDeleting > newsCountAfterDeleting);

	}

	// @Test
	public void testLoadById() throws DaoException {
		News news = new News();
		news = newsDao.loadById(new Long(1));
		Assert.assertNotNull(news.getFullText());
		Assert.assertNotNull(news.getShortText());
		Assert.assertNotNull(news.getTitle());
		Assert.assertNotNull(news.getCreationDate());
		Assert.assertNotNull(news.getId());
		Assert.assertNotNull(news.getModificationDate());

	}

	// @Test
	public void testLoadNewsVo() throws DaoException {
		NewsVO newsVo = new NewsVO();
		newsVo = newsDao.loadNewsVo(new Long(1));
		Assert.assertNotNull(newsVo.getAuthor());
		Assert.assertNotNull(newsVo.getComments());
		Assert.assertNotNull(newsVo.getNews());
		Assert.assertNotNull(newsVo.getTags());
	}

	// @Test
	public void testLoadAll() throws DaoException {
		List<News> newsList = new ArrayList<News>();
		newsList = newsDao.loadAll();
		Assert.assertEquals(3, newsList.size());
	}

	private News buildNews() {
		News news = new News();
		news.setCreationDate(new Timestamp(new Date().getTime()));
		news.setFullText("test full text");
		news.setModificationDate(new Date(new Date().getTime()));
		news.setShortText("test short text");
		news.setTitle("test title");
		news.setId(new Long(1));
		return news;
	}

	private void assertNews(News expected, News actual) throws ParseException {

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date formattedExpectedsNewsModificationDate = formatter.parse(formatter
				.format(expected.getModificationDate()));

		Date formattedExpectedCreationDate = formatter.parse(formatter
				.format(expected.getCreationDate()));

		Assert.assertEquals(expected.getFullText(), actual.getFullText());
		Assert.assertEquals(expected.getShortText(), actual.getShortText());
		Assert.assertEquals(expected.getTitle(), actual.getTitle());
		Assert.assertEquals(formattedExpectedCreationDate.getTime(), actual
				.getCreationDate().getTime());
		Assert.assertEquals(expected.getId(), actual.getId());

		Assert.assertEquals(formattedExpectedsNewsModificationDate.getTime(),
				actual.getModificationDate().getTime());
	}
}
