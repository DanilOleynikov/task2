package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/tag-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/tag-dataset.xml" }, type = DatabaseOperation.DELETE)
public class HibernateTagDaoImplTest {

	@Autowired
	private TagDao tagDao;

	@Test
	public void testAdd() throws DaoException {

		Tag expectedTag = new Tag();
		expectedTag.setName("test");
		Long tagId = tagDao.add(expectedTag);
		Tag actualTag = new Tag();
		actualTag = tagDao.loadById(tagId);
		assertTags(actualTag, expectedTag);

	}

	@Test
	public void testDelete() throws DaoException {
		List<Tag> tagListBeforeDeleting = new ArrayList<Tag>();
		List<Tag> tagListAfterDeleting = new ArrayList<Tag>();

		tagListBeforeDeleting = tagDao.loadAll();
		tagDao.delete(new Long(1));
		tagListAfterDeleting = tagDao.loadAll();

		Assert.assertTrue(tagListBeforeDeleting.size() > tagListAfterDeleting
				.size());
	}

	@Test
	public void testUpdate() throws DaoException {

		Tag expectedTag = new Tag();
		expectedTag.setName("test");
		expectedTag.setId(new Long(1));
		tagDao.update(expectedTag);
		Tag actualTag = new Tag();
		actualTag = tagDao.loadById(new Long(1));
		assertTags(actualTag, expectedTag);

	}

	@Test
	public void testLoadById() throws DaoException {
		Tag tag = new Tag();
		tag = tagDao.loadById(new Long(1));
		Assert.assertEquals(tag.getName(), "yolo123");
		Assert.assertEquals(tag.getId(), new Long(1));
	}

	@Test
	public void testLoadAll() throws DaoException {

		List<Tag> tagList = new ArrayList<Tag>();
		tagList = tagDao.loadAll();
		Assert.assertEquals(tagList.size(), 4);
	}

	private void assertTags(Tag actualTag, Tag expectedTag) {
		Assert.assertEquals(actualTag.getName(), expectedTag.getName());
		Assert.assertEquals(actualTag.getId(), expectedTag.getId());
	}
}
