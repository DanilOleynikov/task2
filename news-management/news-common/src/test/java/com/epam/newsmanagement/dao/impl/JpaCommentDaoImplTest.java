package com.epam.newsmanagement.dao.impl;

import java.util.Date;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/comment-dataset.xml")
@DatabaseTearDown(value = "classpath:com/epam/newsmanagement/dao/impl/comment-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class JpaCommentDaoImplTest {

	@Autowired
	private CommentDao commentDao;

	// @Test
	public void testAdd() throws DaoException {

		Comment comment = new Comment();
		comment.setCreationTime(new Date());
		comment.setNewsId(new Long(1));
		comment.setText("test");

		Long commentId = commentDao.add(comment);
		Comment actualComment = new Comment();
		actualComment = commentDao.loadById(commentId);
		Assert.assertNotNull(actualComment.getText());
		Assert.assertNotNull(actualComment.getCreationTime());
		Assert.assertNotNull(actualComment.getId());
		Assert.assertNotNull(actualComment.getNewsId());

	}

	// @Test
	public void testDelete() throws DaoException {
		commentDao.delete(new Long(1));
		Comment actualComment = new Comment();
		actualComment = commentDao.loadById(new Long(1));
		Assert.assertNull(actualComment);
	}

	// @Test
	public void testDeleteByNewsId() throws DaoException {
		commentDao.delete(new Long(1));
		Comment actualComment = commentDao.loadById(new Long(1));
		Assert.assertNull(actualComment);
	}

	// @Test
	public void testUpdate() throws DaoException {

		Comment comment = new Comment();
		comment.setCreationTime(new Date());
		comment.setId(new Long(1));
		comment.setNewsId(new Long(1));
		comment.setText("test");

		commentDao.update(comment);
		Comment actualComment = new Comment();
		actualComment = commentDao.loadById(new Long(1));

		Assert.assertEquals(actualComment.getText(), "test");
		Assert.assertEquals(actualComment.getId(), new Long(1));
		Assert.assertEquals(actualComment.getNewsId(), new Long(1));
		Assert.assertEquals(actualComment.getCreationTime(),
				comment.getCreationTime());
	}

	// @Test
	public void testLoadById() throws DaoException {

		Comment actualComment = new Comment();
		actualComment = commentDao.loadById(new Long(1));
		Assert.assertNotNull(actualComment.getText());
		Assert.assertNotNull(actualComment.getCreationTime());
		Assert.assertNotNull(actualComment.getId());
		Assert.assertNotNull(actualComment.getNewsId());
	}

}
