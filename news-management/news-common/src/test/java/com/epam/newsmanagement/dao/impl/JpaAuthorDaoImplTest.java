package com.epam.newsmanagement.dao.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml" }, type = DatabaseOperation.DELETE)
public class JpaAuthorDaoImplTest {

	@Autowired
	private AuthorDao authorDao;

	// @Test
	public void testUpdateAuthorName() throws DaoException {

		String nameToUpdate = "test";
		authorDao.updateAuthorName(new Long(1), nameToUpdate);
		Author author = new Author();
		author = authorDao.loadById(new Long(1));
		Assert.assertEquals(nameToUpdate, author.getName());

	}

	// @Test
	public void testAdd() throws DaoException {

		List<Author> authorListBeforeAdding = new ArrayList<Author>();
		authorListBeforeAdding = authorDao.loadAll();
		Author author = new Author();
		author.setExpiredTime(new Date());
		author.setName("test");
		authorDao.add(author);
		List<Author> authorListAfterAdding = new ArrayList<Author>();
		authorListAfterAdding = authorDao.loadAll();
		Assert.assertTrue(authorListBeforeAdding.size() < authorListAfterAdding
				.size());
	}

	// @Test
	public void testUpdate() throws DaoException, ParseException {

		Author author = new Author();
		author.setExpiredTime(new Date());
		author.setName("test");
		author.setId(new Long(1));
		authorDao.update(author);
		Author actualAuthor = new Author();
		actualAuthor = authorDao.loadById(new Long(1));
		assertAuthor(author, actualAuthor);

	}

	// @Test
	public void testLoadById() throws DaoException {
		Author author = new Author();
		author = authorDao.loadById(new Long(1));
		Assert.assertEquals(author.getName(), "Danil");
		Assert.assertEquals(author.getExpiredTime().toString(),
				"Sat Aug 15 00:00:00 EEST 2015");

	}

	// @Test
	public void testLoadAll() throws DaoException {
		List<Author> authorList = new ArrayList<Author>();
		authorList = authorDao.loadAll();
		Assert.assertEquals(3, authorList.size());
	}

	// @Test
	public void testLoadAllNonExpired() throws DaoException {
		List<Author> nonExpiredAuthorList = new ArrayList<Author>();
		nonExpiredAuthorList = authorDao.loadAllNonExpired();
		Assert.assertEquals(1, nonExpiredAuthorList.size());

	}

	private void assertAuthor(Author expected, Author actual)
			throws ParseException {

		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getExpiredTime(), actual.getExpiredTime());
		Assert.assertEquals(expected.getId(), actual.getId());
	}

}
