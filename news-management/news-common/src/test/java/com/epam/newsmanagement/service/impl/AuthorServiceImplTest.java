package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * This class is testing
 * {@link com.epam.newsmanagement.service.impl.AuthorServiceImpl} class. Here we
 * are using Mockito framework and mocked objects. In our case we have mocked
 * object of {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl} class
 * that then set to real object of
 * {@link com.epam.newsmanagement.service.impl.AuthorServiceImpl}; NOTE: all
 * methods that are testing here are not calling during tests, they are calling
 * on mocked objects, so there are NO interactions with database during test
 * invocation. So we can only check if necessary method was called and what
 * value does it return.
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	private JdbcAuthorDaoImpl authorDao;
	private AuthorServiceImpl authorService;

	/**
	 * This method is executing before test methods. It create object of
	 * {@link com.epam.newsmanagement.service.impl.AuthorServiceImpl} class,
	 * also as mocked object of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl} class.
	 * 
	 */
	@Before
	public void setUp() {
		authorDao = Mockito.mock(JdbcAuthorDaoImpl.class);
		authorService = new AuthorServiceImpl();
		authorService.setAuthorDao(authorDao);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#add(Author)}
	 * invoked) and what value does it return.
	 */
	@Test
	public void testAdd() throws DaoException, ServiceException {
		Author dummyAuthor = new Author();
		Long dummyId = new Long(1);
		when(authorService.add(dummyAuthor)).thenReturn(dummyId);

		Long actual = authorService.add(dummyAuthor);
		Assert.assertEquals(dummyId, actual);
		verify(authorDao, atLeastOnce()).add(dummyAuthor);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#update(Author)}
	 * invoked).
	 */
	@Test
	public void testUpdate() throws ServiceException, DaoException {
		Author dummyAuthor = new Author();
		authorService.update(dummyAuthor);
		verify(authorDao, atLeastOnce()).update(dummyAuthor);

	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#loadById(Long)}
	 * invoked) and what value does it return.
	 */
	@Test
	public void testLoadById() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		Author dummyAuthor = new Author();
		Author expected = new Author();

		when(authorService.loadById(dummyId)).thenReturn(dummyAuthor);
		expected = authorService.loadById(dummyId);

		verify(authorDao, atLeastOnce()).loadById(dummyId);
		Assert.assertEquals(expected, dummyAuthor);

	}

	/**
	 * 
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#loadAll()}
	 * invoked) and what valued does it return.
	 */
	@Test
	public void testLoadAll() throws ServiceException, DaoException {
		List<Author> dummyAuthorList = new ArrayList<Author>();
		List<Author> expectedAuthorList = new ArrayList<Author>();

		when(authorService.loadAll()).thenReturn(dummyAuthorList);
		expectedAuthorList = authorService.loadAll();

		verify(authorDao, atLeastOnce()).loadAll();
		Assert.assertEquals(expectedAuthorList, dummyAuthorList);
	}
}
