DELETE FROM comments;
DELETE FROM news_author;
DELETE FROM news_tag;
DELETE FROM tag;
DELETE FROM author;
DELETE FROM news;
DELETE FROM roles;
DELETE FROM users;

INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Afghan hospital bombing','The medical charity MSF has condemned deadly air strikes on hospital in the Afghan city of Kunduz.','Medecins Sans Frontieres said at least nine of its staff were killed in the attack. Many are unaccounted for.It said the strikes continued for more than 30 minutes after US and Afghan authorities were told of its location.US forces were carrying out air strikes at the time. The Nato alliance has admitted the clinic may have been hit.At least 37 people were seriously injured, 19 of them MSF staff.More than 100 patients were in the hospital, along with relatives and carers; it is not known how many of them were killed.MSF says that all parties to the conflict, including Kabul and Washington, had been told the precise GPS co-ordinates of the hospital in Kunduz on many occasions, including on 29 September.After staff at the hospital became aware of the aerial bombardment in the early hours of Saturday morning, US and Afghan military officials were again informed, MSF said.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Federal agents find six weapon','The gunman who killed nine people in a shooting rampage at a college in Oregon had 13 weapons.','Dressed in a flak jacket, Chris Harper Mercer brought six guns to Umpqua Community College in Roseburg and opened fire on Thursday morning.He was killed by police in a gun battle and another seven weapons were found at his home. All 13 were bought legally.Police have released the names of the victims, who ranged in age from 18 to 67. The oldest was a teacher.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('12-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Ukraine weapons pullout','Ukraine crisis: Government troops and rebels to start withdrawing light weapons','The move was announced at a meeting of leaders from Ukraine, Russia, Germany and France in Paris on Friday.Weapons of less than 100mm (4in) calibre should be pulled out in stages and the process completed within 41 days, Ukraine said.Moscow denies sending troops and heavy weapons to the pro-Russian separatists.However, the Kremlin admits that Russian "volunteers" are fighting alongside the rebels in the Donetsk and Luhansk regions.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('13-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Porsche Cayman Black Edition','Porsche introduced—or reintroduced, more accurately—special Black Edition models of the 911 Boxter','As with its Black Edition brethren, the Cayman Black Edition is based on the non-S model, which means that a 275-hp 2.7-liter flat-6 resides just aft of the passengers. In addition to the Caymans standard features, the Black Edition features 20-inch Carrera Classic wheels, bixenon dynamic headlamps, front and rear park assist, and a choice of either black or metallic black paint. Interior amenities include an upgraded audio system, navigation, dual-zone climate control, heated seats, a sport steering wheel, Black Edition logos on the doorsills, an auto-dimming rearview mirror, and Porsche crests embossed in the headrests.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Mitsubishi Lancer: Improved','The trouble with that old trope about “breaking through the noise” is that it acknowledges context.','For example, its at least possible to raise your voice loud enough to drown out others—if you are in the same room. Mitsubishi is trying to do just that in the compact-car segment with some updates for its Lancer sedan, but in spite of a rather reasoned argument built on generous standard features and affordable pricing, will it be heard? After all, having sold just 16,495 Lancers last year—compared to 325,981 Honda Civics moved during the same period—is Mitsubishi even in the same room as its myriad compact-sedan competitors?',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'Honda Reveals FCV Vehicle',' Hondas new hydrogen-fuel-cell sedan has now shown in production form, ahead of its in-the-flesh.','The real car—the red one—has a less exotic front end, with a more traditional grille. It also sprouts door handles (very practical), an arc-shaped piece of chrome that follows the roofline, and redesigned front fender vents. The greenhouse takes on more of a Crosstour-like shape, which is a little unfortunate.Honda says the production car will offer a cruising range of more than 435 miles, but thats for the Japanese market, which has a different test cycle than we do.For us, it will be closer to the 300 miles that Honda was talking at the time of the FCV Concept. The company also boasts that it was able to package all the powertrain components outside of the passenger compartment, allowing the FCV to seat five.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('09-AUG-15','DD-MON-RR'),1);
INSERT INTO news (NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,VERSION_OPT_LOCK) VALUES(news_seq.NEXTVAL,'S.Williams win as Muguruza out','Venus Williams wins the Wuhan Open title as final opponent Garbine Muguruza with an ankle injury.','Both players came into the tournament less than fully fit, with Williams, 35, struggling with a thigh problem.The American, a seven-time Grand Slam champion, used her experience to good effect in the first set of the final.She broke Wimbledon finalist Muguruza twice in the second set before the 21-year-old Spaniard withdrew. Both players exchanged breaks of serve before world number 25 Williams closed out the set.',to_timestamp('12-AUG-16 06.15.18.007000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('07-AUG-15','DD-MON-RR'),1);


INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,1,'So pitty. Hope they will solve their problems in Afghan.',to_timestamp('13-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,2,'This is terrible event. How does it happened?',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,3,'Again strange events in Ukraine...',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,4,'Porshe became too expensive. Im collect some money.',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,5,'Finally they pubslied new version of Evo.',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,6,'This Japan company is very specific.',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,7,'Congratulations!',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,1,'Bad news((',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO comments (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES(comments_seq.NEXTVAL,1,'Again terrorist here',to_timestamp('14-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));


INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Politics');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'War');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Cars');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Sport');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Tennis');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Ukraine');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Porshe');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Mitsubishi');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Tuning');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Afghan');

INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Criminal');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Bandit');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Vehicle');
INSERT INTO tag (TAG_ID,TAG_NAME) VALUES(tag_seq.NEXTVAL,'Honda');




INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Joe Lorio',to_timestamp('14-AUG-16 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Alex Stolkosa',to_timestamp('05-SEP-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Steve Siller',to_timestamp('10-SEP-18 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Kevin A.',to_timestamp('21-NOV-15 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Aaron Robinson',to_timestamp('15-AUG-14 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Kirk Seaman',to_timestamp('15-AUG-16 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Michael Taylor',to_timestamp('15-AUG-17 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));

INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Eugenii Hook',to_timestamp('15-AUG-16 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));
INSERT INTO author (AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(author_seq.NEXTVAL,'Vera Kostamo',to_timestamp('15-AUG-16 06.15.18.007000000','DD-MON-RR HH.MI.SSXFF AM'));

INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(1,1);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(2,2);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(3,3);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(4,4);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(5,5);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(6,6);
INSERT INTO news_author (NEWS_ID,AUTHOR_ID) VALUES(7,7);


INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(1,1);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(1,2);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(1,10);

INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(2,11);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(2,12);


INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(3,1);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(3,6);


INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(4,3);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(4,7);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(4,9);

INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(5,3);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(5,8);

INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(6,3);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(6,13);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(6,14);

INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(7,4);
INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES(7,5);


INSERT INTO USERS (USER_ID,USER_NAME,LOGIN,PASSWORD) VALUES(USERS_SEQ.NEXTVAL,'adminUserName','admin','21232f297a57a5a743894a0e4a801fc3');

INSERT INTO ROLES (USER_ID,ROLE_NAME) VALUES(1,'ROLE_ADMIN');





