/***********************************************************************/
/*                  DROPING(EXISTING) TABLES                          */
/*********************************************************************/
/*DROP TABLE NEWS_AUTHOR IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE NEWS_AUTHOR CASCADE CONSTRAINTS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE NEWS_TAG IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE NEWS_TAG CASCADE CONSTRAINTS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE COMMENTS IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE COMMENTS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE TAG IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE TAG';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE AUTHOR IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE AUTHOR';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE NEWS IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE NEWS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE ROLES IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE ROLES';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/*DROP TABLE USERS IF IT EXISTS*/
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE USERS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
/***********************************************************************/
/*                  DROPING/CREATING SEQUENCES                        */
/*********************************************************************/
/*CREATE NEWS_SEQ IF IT DOESN'T EXIST */
DECLARE
  v_dummy NUMBER;
BEGIN
  -- try to find sequence in data dictionary
  SELECT 1
  INTO v_dummy
  FROM user_sequences
  WHERE sequence_name = 'NEWS_SEQ';
  -- if sequence found, do nothing
EXCEPTION
  WHEN no_data_found THEN
    -- sequence not found, create it
    EXECUTE IMMEDIATE 'create sequence NEWS_SEQ START WITH 1 MINVALUE 0';
END;
/
/*CREATE COMMENTS_SEQ IF IT DOESN'T EXIST */
DECLARE
  v_dummy NUMBER;
BEGIN
  -- try to find sequence in data dictionary
  SELECT 1
  INTO v_dummy
  FROM user_sequences
  WHERE sequence_name = 'COMMENTS_SEQ';
  -- if sequence found, do nothing
EXCEPTION
  WHEN no_data_found THEN
    -- sequence not found, create it
    EXECUTE IMMEDIATE 'create sequence COMMENTS_SEQ START WITH 1 MINVALUE 0';
END;
/
/*CREATE AUTHOR_SEQ IF IT DOESN'T EXIST */
DECLARE
  v_dummy NUMBER;
BEGIN
  -- try to find sequence in data dictionary
  SELECT 1
  INTO v_dummy
  FROM user_sequences
  WHERE sequence_name = 'AUTHOR_SEQ';
  -- if sequence found, do nothing
EXCEPTION
  WHEN no_data_found THEN
    -- sequence not found, create it
    EXECUTE IMMEDIATE 'create sequence AUTHOR_SEQ START WITH 1 MINVALUE 0';
END;
/
/*CREATE TAG_SEQ IF IT DOESN'T EXIST */
DECLARE
  v_dummy NUMBER;
BEGIN
  -- try to find sequence in data dictionary
  SELECT 1
  INTO v_dummy
  FROM user_sequences
  WHERE sequence_name = 'TAG_SEQ';
  -- if sequence found, do nothing
EXCEPTION
  WHEN no_data_found THEN
    -- sequence not found, create it
    EXECUTE IMMEDIATE 'create sequence TAG_SEQ START WITH 1 MINVALUE 0';
END;
/
/*CREATE USERS_SEQ IF IT DOESN'T EXIST */
DECLARE
  v_dummy NUMBER;
BEGIN
  -- try to find sequence in data dictionary
  SELECT 1
  INTO v_dummy
  FROM user_sequences
  WHERE sequence_name = 'USERS_SEQ';
  -- if sequence found, do nothing
EXCEPTION
  WHEN no_data_found THEN
    -- sequence not found, create it
    EXECUTE IMMEDIATE 'create sequence USERS_SEQ START WITH 1 MINVALUE 0';
END;
/

CREATE TABLE NEWS(
NEWS_ID number(20) NOT NULL PRIMARY KEY,
TITLE nvarchar2(30) NOT NULL,
SHORT_TEXT nvarchar2(100) NOT NULL,
FULL_TEXT nvarchar2(2000) NOT NULL,
CREATION_DATE TIMESTAMP NOT NULL,
MODIFICATION_DATE DATE NOT NULL
);
/
CREATE TABLE COMMENTS(
COMMENT_ID number(20) NOT NULL PRIMARY KEY,
NEWS_ID number (20) NOT NULL,
COMMENT_TEXT nvarchar2(100) NOT NULL,
CREATION_DATE TIMESTAMP NOT NULL,
CONSTRAINT comments_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID)
);
/
CREATE TABLE AUTHOR(
AUTHOR_ID number(20) NOT NULL PRIMARY KEY,
AUTHOR_NAME nvarchar2(30) NOT NULL,
EXPIRED TIMESTAMP
);
/
CREATE TABLE NEWS_AUTHOR(
NEWS_ID number(20) NOT NULL,
AUTHOR_ID number(20) NOT NULL,
CONSTRAINT news_author_author_fk FOREIGN KEY (AUTHOR_ID) REFERENCES AUTHOR(AUTHOR_ID),
CONSTRAINT news_author_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID)
);
/
CREATE TABLE TAG(
TAG_ID number(20) NOT NULL PRIMARY KEY,
TAG_NAME nvarchar2(30) NOT NULL
);
/
CREATE TABLE NEWS_TAG(
NEWS_ID number(20) NOT NULL,
TAG_ID NUMBER(20) NOT NULL,
CONSTRAINT news_tag_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID),
CONSTRAINT news_tag_tag_fk FOREIGN KEY (TAG_ID) REFERENCES TAG(TAG_ID)
);
/
CREATE TABLE USERS(
USER_ID number(20) NOT NULL PRIMARY KEY,
USER_NAME nvarchar2(50) NOT NULL,
LOGIN varchar2(30) NOT NULL,
PASSWORD varchar2(30) NOT NULL
);
/
CREATE TABLE ROLES(
USER_ID number(20) NOT NULL,
ROLE_NAME varchar2(50) NOT NULL,
CONSTRAINT roles_users_fk FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID)
);
/