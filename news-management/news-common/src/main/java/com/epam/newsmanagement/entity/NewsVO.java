package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * NewsVO(Value object) is a class that containing NEWS object with it set of
 * TAGS and COMMENTS and AUTHOR of NEWS;
 */
public class NewsVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private News news;
	private Set<Tag> tags;
	private Author author;
	private Set<Comment> comments;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * This method is using for string representation of NewsVO object.
	 */
	@Override
	public String toString() {
		return new StringBuilder("NewsMessage [news=").append(news)
				.append(", tags=").append(tags).append(", author=")
				.append(author).append(", comments=").append(comments)
				.append("]").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

}
