package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * NewsServiceImpl is an implementation of
 * {@link com.epam.newsmanagement.service.NewsService} interface. At this point
 * this class are injected by {@link com.epam.newsmanagement.dao.NewsDao} object
 * via Spring. So at now it have the same functionality. But also it can be
 * expanded by another functional methods. NOTE:in all implemented methods added
 * checking for passing null values;
 */

@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService {

	private NewsDao newsDao;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.NewsDao};
	 * 
	 * @param newsDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.NewsDao}.
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	@Override
	public List<Long> loadAllNewsId() throws ServiceException {
		List<Long> newsId = new ArrayList<Long>();
		try {
			newsId = newsDao.loadAllNewsId();
		} catch (DaoException e) {

			throw new ServiceException(e.getMessage(), e);
		}
		return newsId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdSearchedByCriteria}
	 */
	@Override
	public List<Long> loadNewsIdSearchedByCriteria(SearchCriteria sc, Long page)
			throws ServiceException {
		List<Long> newsId = new ArrayList<Long>();

		try {
			newsId = newsDao.loadNewsIdSearchedByCriteria(sc, page);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage(), e);

		}
		return newsId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate}
	 */
	@Override
	public List<Long> loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(
			Long pageNumber) throws ServiceException {
		List<Long> newsId = new ArrayList<Long>();

		if (pageNumber != null) {
			try {
				newsId = newsDao
						.loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(pageNumber);
			} catch (DaoException e) {
				throw new ServiceException("Can't load news by specified page:"
						+ e);
			}
		}

		return newsId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadListOfNewsVoByNewsId}
	 */
	@Override
	public List<NewsVO> loadListOfNewsVoByNewsId(List<Long> newsId)
			throws ServiceException {
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();

		if (newsId != null) {
			try {
				newsVoList = newsDao.loadListOfNewsVoByNewsId(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return newsVoList;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#getNewsCount}
	 */
	@Override
	public Long getNewsCount() throws ServiceException {
		Long newsCount = new Long(0);
		try {
			newsCount = newsDao.getNewsCount();
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return newsCount;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 */

	@Override
	public Long add(News news) throws ServiceException {

		Long newsId = new Long(0);
		if (news != null) {
			try {
				newsId = newsDao.add(news);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return newsId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)}
	 */
	@Override
	public void update(News news) throws ServiceException {
		if (news != null) {
			try {
				newsDao.update(news);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)}
	 */
	@Override
	public void delete(Long newsId) throws ServiceException {
		if (newsId != null) {
			try {
				newsDao.delete(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)}
	 */
	@Override
	public News loadById(Long newsId) throws ServiceException {
		News news = new News();
		if (newsId != null) {
			try {
				news = newsDao.loadById(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return news;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)}
	 */

	@Override
	public NewsVO loadNewsVo(Long newsId) throws ServiceException {
		NewsVO newsVo = new NewsVO();
		if (newsId != null) {
			try {
				newsVo = newsDao.loadNewsVo(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}

		return newsVo;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 */
	@Override
	public List<News> loadAll() throws ServiceException {
		List<News> news = null;
		try {
			news = newsDao.loadAll();
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return news;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#findByCriteria(SearchCriteria)}
	 */
	@Override
	public List<News> findByCriteria(SearchCriteria sc) throws ServiceException {
		List<News> resultNews = new ArrayList<News>();
		try {
			resultNews = newsDao.findByCriteria(sc);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return resultNews;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 */
	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws ServiceException {

		if (newsId != null && authorId != null) {
			try {
				newsDao.addNewsAuthorLink(newsId, authorId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws ServiceException {
		if (newsId != null && tagId != null) {
			try {
				newsDao.addNewsTagLink(newsId, tagId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)}
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long... tagId)
			throws ServiceException {

		if (newsId != null && tagId != null) {
			try {
				newsDao.addNewsTagLink(newsId, tagId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 */
	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId)
			throws ServiceException {

		if (newsId != null) {
			try {
				newsDao.deleteNewsAuthorLinkByNewsId(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 */
	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws ServiceException {
		if (authorId != null) {
			try {
				newsDao.deleteNewsAuthorLinkByAuthorId(authorId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 */
	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws ServiceException {

		if (newsId != null) {
			try {
				newsDao.deleteNewsTagLinkByNewsId(newsId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 */
	@Override
	public void deleteNewsTagLink(Long tagId) throws ServiceException {
		if (tagId != null) {
			try {
				newsDao.deleteNewsTagLink(tagId);
			} catch (DaoException e) {
				throw new ServiceException(e.getMessage(), e);
			}

		}

	}
}
