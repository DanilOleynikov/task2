package com.epam.newsmanagement.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is using to provide user with such functionality as closing
 * resources that was opened according to get access to database, also as
 * converting different data from one state to another one.
 */
public class DatabaseUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DatabaseUtil.class);

	/**
	 * This method is using for releasing resources such as connection and
	 * statement.
	 * 
	 * @param connection
	 *            connection object that is need to be released to pool.
	 * @param statement
	 *            statement object that is need to be closed.
	 * @param dataSource
	 *            datasource object with whom programm is working.
	 * @throws DaoException
	 *             DaoException arise if it occured.
	 */
	public static void closeConnectionStatement(Connection connection,
			Statement statement, DataSource dataSource) throws DaoException {

		if (connection != null) {
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			} catch (SQLException e) {
				LOGGER.error(
						"Exception acquired when trying to close connection in closeConnectionStatement method: ",
						e.getMessage());
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.error(
						"Exception acquired when trying to close statement in closeConnectionStatement method: ",
						e.getMessage());
			}
		}

	}

	/**
	 * This method is using for releasing resources such as connection,
	 * statement and resultset;
	 * 
	 * @param connection
	 *            connection object that is need to be released to pool.
	 * @param statement
	 *            statement object that is need to be closed.
	 * @param dataSource
	 *            datasource object with whom programm is working.
	 * @param resultSet
	 *            resultset object that is need to be closed;
	 * @throws DaoException
	 *             DaoException arise if it occured.
	 */

	public static void closeConnectionStatementResultSet(Connection connection,
			Statement statement, ResultSet resultSet, DataSource dataSource)
			throws DaoException {

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.error(
						"Exception acquired when trying to close resultSet in closeConnectionStatementResultSet method:",
						e.getMessage());
			}
		}

		if (connection != null) {
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			} catch (SQLException e) {
				LOGGER.error(
						"Exception acquired when trying to release connection in closeConnectionStatementResultSet method: ",
						e.getMessage());
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.error(
						"Exception acquired when trying to close statement in closeConnectionStatementResultSet method: ",
						e.getMessage());
			}
		}

	}
}
