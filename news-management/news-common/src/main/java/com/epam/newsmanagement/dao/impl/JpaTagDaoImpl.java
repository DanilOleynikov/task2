package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of {@link com.epam.newsmanagement.dao.TagDao}
 * interface. In order to interact with database we are using JPA with
 * EclipseLink as PersistenceProvider.
 */
@Transactional
public class JpaTagDaoImpl implements TagDao {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#add(Tag)}
	 * method using JPA(EclipseLink);
	 */

	@Override
	public Long add(Tag tag) throws DaoException {
		try {
			em.persist(tag);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tag.getId();
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#delete(Long)}
	 * method using JPA(EclipseLink);
	 */

	@Override
	public void delete(Long tagId) throws DaoException {
		Tag tag;
		tag = loadById(tagId);
		if (tag != null) {
			try {
				em.remove(tag);
			} catch (PersistenceException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}

		}
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#update(Tag)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void update(Tag tag) throws DaoException {
		try {
			em.merge(tag);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.TagDao#loadById(Long)} method using
	 * JPA(EclipseLink);
	 */
	@Override
	public Tag loadById(Long tagId) throws DaoException {
		Tag tag = new Tag();
		try {
			tag = em.find(Tag.class, tagId);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tag;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#loadAll()}
	 * method using JPA(EclipseLink);
	 */

	@Override
	public List<Tag> loadAll() throws DaoException {
		List<Tag> tags = new ArrayList<Tag>();
		try {
			tags = em.createNamedQuery("Tag.LoadAll", Tag.class)
					.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tags;
	}
}
