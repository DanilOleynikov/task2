package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.util.QueryBuilder;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of {@link com.epam.newsmanagement.dao.NewsDao}
 * interface through Hibernate. In order to achieve necessary functionality we
 * using {@link org.hibernate.SessionFactory} that allow us to retieve certain
 * information from database. In order to make this class more easier, we
 * injected some dao's.
 */
@Transactional
public class HibernateNewsDaoImpl implements NewsDao {

	private static final String SQL_LOAD_IDS_OF_NEWS_ON_SPECIFIC_PAGE = "select * from (select a.*, ROWNUM rnum from (SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC) a where rownum <= 3 + 3*?) where rnum >= 1 + 3*?";
	private static final String SQL_LOAD_ALL_NEWS_ID = "SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_LOAD_SINGLE_NEWS_MESSAGE = "SELECT NEWS.NEWS_ID,COMMENTS.COMMENT_ID,TAG.TAG_ID, AUTHOR.AUTHOR_ID FROM NEWS_TAG INNER JOIN NEWS  ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID  INNER JOIN TAG  ON TAG.TAG_ID = NEWS_TAG.TAG_ID  INNER JOIN NEWS_AUTHOR  ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID  INNER JOIN AUTHOR  ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID WHERE NEWS.NEWS_ID = ? GROUP BY NEWS.NEWS_ID,COMMENTS.COMMENT_ID,TAG.TAG_ID,AUTHOR.AUTHOR_ID";

	private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO news_author VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "DELETE FROM news_author WHERE news_id=?";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID = "DELETE FROM news_author WHERE author_id=?";

	private static final String SQL_ADD_NEWS_TAG = "INSERT INTO news_tag VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "DELETE FROM news_tag WHERE tag_id=?";
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "DELETE FROM news_tag WHERE news_id=?";

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CommentDao commentDao;
	@Autowired
	private AuthorDao authorDao;
	@Autowired
	private TagDao tagDao;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadAllNewsId} method using
	 * Hibernate.
	 */
	@Override
	public List<Long> loadAllNewsId() throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		List<Object[]> rows = new ArrayList<Object[]>();
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_LOAD_ALL_NEWS_ID);
			rows = query.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(Long.valueOf(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdSearchedByCriteria}
	 * method using Hibernate.
	 */
	@Override
	public List<Long> loadNewsIdSearchedByCriteria(SearchCriteria sc, Long page)
			throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		String criteriaQuery = QueryBuilder.buildQueryFromSearchCriteria(sc);
		List<Object[]> rows = new ArrayList<Object[]>();
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					criteriaQuery);
			query.setLong(0, page);
			query.setLong(1, page);
			rows = query.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(Long.valueOf(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate}
	 * method using Hibernate.
	 */
	@Override
	public List<Long> loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(
			Long pageNumber) throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_LOAD_IDS_OF_NEWS_ON_SPECIFIC_PAGE);
			query.setLong(0, pageNumber);
			query.setLong(1, pageNumber);
			rows = query.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(Long.valueOf(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadListOfNewsVoByNewsId}
	 * method using Hibernate.
	 */
	@Override
	public List<NewsVO> loadListOfNewsVoByNewsId(List<Long> newsId)
			throws DaoException {
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		for (Long l : newsId) {
			NewsVO newsVo = new NewsVO();
			newsVo = loadNewsVo(l);
			newsVoList.add(newsVo);
		}
		return newsVoList;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#getNewsCount} method using
	 * Hibernate.
	 */
	@Override
	public Long getNewsCount() throws DaoException {
		Long newsCount = new Long(0);

		try {
			newsCount = (Long) sessionFactory.getCurrentSession()
					.createCriteria(News.class)
					.setProjection(Projections.rowCount()).uniqueResult();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		return newsCount;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 * method using Hibernate.
	 */
	@Override
	public Long add(News news) throws DaoException {
		try {
			sessionFactory.getCurrentSession().save(news);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return news.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)} method using
	 * Hibernate.
	 */

	@Override
	public void update(News news) throws DaoException {
		try {
			sessionFactory.getCurrentSession().update(news);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)} method using
	 * Hibernate.
	 */
	@Override
	public void delete(Long newsId) throws DaoException {

		News news = new News();
		news = loadById(newsId);
		if (news != null) {
			try {
				sessionFactory.getCurrentSession().delete(news);
			} catch (HibernateException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}

		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)} method using
	 * Hibernate.
	 */

	@Override
	public News loadById(Long newsId) throws DaoException {

		News news = new News();
		try {
			news = (News) sessionFactory.getCurrentSession().get(News.class,
					newsId);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		return news;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)} method uisng
	 * Hibernate.
	 */
	@Override
	public NewsVO loadNewsVo(Long newsId) throws DaoException {

		NewsVO newsVo = new NewsVO();
		News news = new News();
		Author author = new Author();
		Set<Comment> comments = new HashSet<Comment>();
		Set<Tag> tags = new HashSet<Tag>();
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_LOAD_SINGLE_NEWS_MESSAGE);
			query.setParameter(0, newsId);
			rows = query.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				Comment comment = new Comment();
				comment = commentDao.loadById(Long.valueOf(row[1].toString()));
				comments.add(comment);

				Tag tag = new Tag();
				tag = tagDao.loadById(Long.valueOf(row[2].toString()));
				tags.add(tag);

				author = authorDao.loadById(Long.valueOf(row[3].toString()));
			}
			news = loadById(newsId);
			newsVo.setNews(news);
		}

		newsVo.setAuthor(author);
		newsVo.setComments(comments);
		newsVo.setTags(tags);
		return newsVo;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 * method using Hibernate.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<News> loadAll() throws DaoException {

		List<News> newsList = new ArrayList<News>();
		try {
			newsList = sessionFactory.getCurrentSession()
					.createCriteria(News.class).list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return newsList;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 * method using Hibernate.
	 */
	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws DaoException {

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_ADD_NEWS_AUTHOR);
			query.setLong(0, newsId);
			query.setLong(1, authorId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 * method using Hibernate.
	 */
	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws DaoException {
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID);
			query.setLong(0, authorId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 * method using Hibernate.
	 */

	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId) throws DaoException {

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			query.setLong(0, newsId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 * method using Hibernate.
	 */

	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws DaoException {
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_ADD_NEWS_TAG);
			query.setLong(0, newsId);
			query.setLong(1, tagId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)
	 * )} method using Hibernate.
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long... tagId) throws DaoException {
		String sqlQuery = QueryBuilder
				.buildQueryForInsertingManyValuesInNewsTagAtOnce(newsId, tagId);
		Query query = null;

		if (sqlQuery.length() != 0) {
			try {
				query = sessionFactory.getCurrentSession().createSQLQuery(
						sqlQuery);
				for (int i = 0; i < tagId.length; i++) {
					query.setLong(2 * i + 1, newsId);
					query.setLong(2 * i + 2, tagId[i]);
				}
				query.executeUpdate();
			} catch (HibernateException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}

		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 * method using Hibernate.
	 */
	@Override
	public void deleteNewsTagLink(Long tagId) throws DaoException {
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_DELETE_NEWS_TAG_BY_TAG_ID);
			query.setLong(0, tagId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 * method using Hibernate.
	 */
	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws DaoException {

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			query.setLong(0, newsId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}
}
