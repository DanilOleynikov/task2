package com.epam.newsmanagement.dao.util;

import java.util.Calendar;
import java.util.TimeZone;

public class DateHelper {
	public static final String DATE_FORMAT = "UTC";
	private static final int MS_PER_OUR = 3600000;
	private static final Calendar UTC_FORMAT_CALENDAR = Calendar
			.getInstance(TimeZone.getTimeZone(DateHelper.DATE_FORMAT));

	private DateHelper() {
	};

	public static String getUtcOffset(TimeZone tZ) {
		StringBuilder utcOffset = new StringBuilder(DATE_FORMAT);
		int offsetHourCounter = (int) Math.ceil(tZ.getRawOffset() / MS_PER_OUR);
		if (offsetHourCounter > 0) {
			utcOffset.append("+");
			utcOffset.append(offsetHourCounter);
		} else if (offsetHourCounter < 0) {
			utcOffset.append(offsetHourCounter);
		}
		return utcOffset.toString();
	}

	public static Calendar getUtcCalendar() {
		return UTC_FORMAT_CALENDAR;
	}
}
