package com.epam.newsmanagement.dao.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ContextProvider {
	private static final String contextPath = "application-context.xml";

	private static ApplicationContext context;
	private static volatile ContextProvider instance;

	private ContextProvider(ApplicationContext ac) {
		context = ac;
	};

	public static ContextProvider getInstance() {
		if (instance == null) {
			System.out.println("instance не создан!");
			synchronized (ContextProvider.class) {
				if (instance == null) {
					System.out.println("instance не создан!Создаем его!");
					instance = new ContextProvider(
							new ClassPathXmlApplicationContext(contextPath));
				}
			}
		}
		return instance;
	}

	public ApplicationContext getContext() {
		return context;
	}
}
