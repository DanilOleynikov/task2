package com.epam.newsmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Tag class is representing tag model for table TAG in database. TAG table
 * fields are TAG_ID;TAG_NAME;
 */
@Entity
@Table(name = "TAG")
@NamedQuery(name = "Tag.LoadAll", query = "SELECT c FROM Tag c")
public class Tag implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAG_ID")
	@SequenceGenerator(name = "TAG_SEQUENCE", sequenceName = "TAG_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQUENCE")
	private Long id;

	@Column(name = "TAG_NAME")
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method is using for string representation of Tag object.
	 */
	@Override
	public String toString() {
		return new StringBuilder("Tag [id=").append(id).append(", name=")
				.append(name).append("]").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
