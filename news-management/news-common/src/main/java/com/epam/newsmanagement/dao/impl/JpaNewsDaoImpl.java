package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.util.QueryBuilder;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of {@link com.epam.newsmanagement.dao.NewsDao}
 * method, that using for interactions with table NEWS in database. It's using
 * JPA with ExlipseLink as persistence provider to connect and retreaved data
 * from database;
 */
@Transactional
public class JpaNewsDaoImpl implements NewsDao {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AuthorDao authorDao;
	@Autowired
	private CommentDao commentDao;
	@Autowired
	private TagDao tagDao;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadAllNewsId} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public List<Long> loadAllNewsId() throws DaoException {
		List<Long> newsId = new ArrayList<Long>();
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = em.createNamedQuery("News.LoadAllNewsId");
			rows = query.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(Long.valueOf(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdSearchedByCriteria}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public List<Long> loadNewsIdSearchedByCriteria(SearchCriteria sc, Long page)
			throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		String sqlQuery = QueryBuilder.buildQueryFromSearchCriteria(sc);
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = em.createNativeQuery(sqlQuery);
			query.setParameter(1, page);
			query.setParameter(2, page);
			rows = query.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(new Long(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public List<Long> loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(
			Long pageNumber) throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = em.createNamedQuery("News.LoadIdsBySpecifiedPage");
			query.setParameter(1, pageNumber);
			query.setParameter(2, pageNumber);
			rows = query.getResultList();

		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				newsId.add(Long.valueOf(row[0].toString()));
			}
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadListOfNewsVoByNewsId}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public List<NewsVO> loadListOfNewsVoByNewsId(List<Long> newsId)
			throws DaoException {
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		if (newsId != null) {
			for (Long l : newsId) {
				NewsVO newsVo = new NewsVO();
				newsVo = loadNewsVo(l);
				newsVoList.add(newsVo);
			}
		}
		return newsVoList;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#getNewsCount} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public Long getNewsCount() throws DaoException {
		Long newsCount = new Long(0);
		try {
			newsCount = (long) em.createNamedQuery("News.LoadAll", News.class)
					.getResultList().size();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		return newsCount;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public Long add(News news) throws DaoException {
		try {
			em.persist(news);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return news.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public void update(News news) throws DaoException {
		try {
			em.merge(news);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public void delete(Long newsId) throws DaoException {
		News news;
		news = loadById(newsId);
		if (news != null) {
			try {
				em.remove(news);
			} catch (PersistenceException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public News loadById(Long newsId) throws DaoException {
		News news = new News();
		try {
			news = em.find(News.class, newsId);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return news;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)} method uisng
	 * JPA(EclipseLink).
	 */

	@Override
	public NewsVO loadNewsVo(Long newsId) throws DaoException {
		NewsVO newsVo = new NewsVO();
		Set<Comment> comments = new HashSet<Comment>();
		Set<Tag> tags = new HashSet<Tag>();
		Author author = new Author();
		List<Object[]> rows = new ArrayList<Object[]>();

		try {
			Query query = em.createNamedQuery("News.LoadSingleNewsMessage");
			query.setParameter(1, newsId);
			rows = query.getResultList();

		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		if (rows != null) {
			for (Object[] row : rows) {
				Comment comment = new Comment();
				comment = commentDao.loadById(Long.valueOf(row[1].toString()));
				comments.add(comment);

				Tag tag = new Tag();
				tag = tagDao.loadById(Long.valueOf(row[2].toString()));
				tags.add(tag);

				author = authorDao.loadById(Long.valueOf(row[3].toString()));
			}
			News news = new News();
			news = loadById(newsId);
			newsVo.setNews(news);
		}
		newsVo.setAuthor(author);
		newsVo.setComments(comments);
		newsVo.setTags(tags);
		return newsVo;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public List<News> loadAll() throws DaoException {

		List<News> news = new ArrayList<News>();
		try {
			news = em.createNamedQuery("News.LoadAll", News.class)
					.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return news;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Author.Add");
			query.setParameter(0, newsId);
			query.setParameter(1, authorId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Author.DeleteByAuthorId");
			query.setParameter(0, authorId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId) throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Author.DeleteByNewsId");
			query.setParameter(0, newsId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Tag.Add");
			query.setParameter(0, newsId);
			query.setParameter(1, tagId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)
	 * )} method using JPA(EclipseLink).
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long... tagId) throws DaoException {

		String sqlQuery = QueryBuilder
				.buildQueryForInsertingManyValuesInNewsTagAtOnce(newsId, tagId);
		Query query = null;

		if (sqlQuery.length() != 0) {
			try {
				query = em.createQuery(sqlQuery);
				for (int i = 0; i < tagId.length; i++) {
					query.setParameter(2 * i + 1, newsId);
					query.setParameter(2 * i + 2, tagId[i]);
				}
				query.executeUpdate();
			} catch (PersistenceException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void deleteNewsTagLink(Long tagId) throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Tag.DeleteByTagId");
			query.setParameter(0, tagId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws DaoException {
		try {
			Query query = em.createNamedQuery("News_Tag.DeleteByNewsId");
			query.setParameter(0, newsId);
			query.executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}
}
