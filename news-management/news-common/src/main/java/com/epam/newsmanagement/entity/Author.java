package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Author class is representing author model for table AUTHOR in database.
 * Author table fields are AUTHOR_ID;AUTHOR_NAME;EXPIRED;
 */

@Entity
@Table(name = "AUTHOR")
@NamedNativeQuery(name = "Author.LoadNonExpired", query = "SELECT * FROM author WHERE EXPIRED>SYSTIMESTAMP AND AUTHOR_ID NOT IN(SELECT AUTHOR_ID FROM NEWS_AUTHOR)")
@NamedQuery(name = "Author.LoadAll", query = "SELECT c FROM Author c")
public class Author implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "AUTHOR_SEQUENCE", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQUENCE")
	@Column(name = "AUTHOR_ID")
	private Long id;

	@Column(name = "AUTHOR_NAME")
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "EXPIRED")
	private Date expiredTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpiredTime() {
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}

	/**
	 * This method is using for string representation of Author object.
	 */
	@Override
	public String toString() {
		return new StringBuilder("Author [id=").append(id).append(", name=")
				.append(name).append(", expiredTime=").append(expiredTime)
				.append("]").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((expiredTime == null) ? 0 : expiredTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expiredTime == null) {
			if (other.expiredTime != null)
				return false;
		} else if (!expiredTime.equals(other.expiredTime))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
