package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is JPA implementation of
 * {@link com.epam.newsmanagement.dao.AuthorDao}, that using EclipseLink as
 * persistence provider. In order to interact with database we are using
 * {@link javax.persistence.EntityManager} object, that allow us to manipulate
 * certain table in database.
 */
@Transactional
public class JpaAuthorDaoImpl implements AuthorDao {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#updateAuthorExpired(Long authorId)}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public void updateAuthorExpired(Long authorId) throws DaoException {
		Author author = new Author();
		author = loadById(authorId);
		author.setExpiredTime(new Date());

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#updateAuthorName(Long authorId, String name)}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public void updateAuthorName(Long authorId, String name)
			throws DaoException {
		Author author = new Author();
		author = loadById(authorId);
		author.setName(name);
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#add(Author)} method using
	 * JPA(EclipseLink).
	 */
	@Override
	public Long add(Author author) throws DaoException {
		try {
			em.persist(author);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return author.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#update(Author)} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public void update(Author author) throws DaoException {
		try {
			em.merge(author);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadById(Long)} method using
	 * JPA(EclipseLink).
	 */
	@Override
	public Author loadById(Long authorId) throws DaoException {
		Author author = new Author();
		try {
			author = em.find(Author.class, authorId);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return author;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.AuthorDao#loadAll()}
	 * method using JPA(EclipseLink).
	 */

	@Override
	public List<Author> loadAll() throws DaoException {
		List<Author> authors = new ArrayList<Author>();
		try {
			authors = em.createNamedQuery("Author.LoadAll", Author.class)
					.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return authors;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadAllNonExpired()} method
	 * using JPA(EclipseLink).
	 */

	@Override
	public List<Author> loadAllNonExpired() throws DaoException {
		List<Author> authors = new ArrayList<Author>();
		try {
			authors = em
					.createNamedQuery("Author.LoadNonExpired", Author.class)
					.getResultList();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return authors;
	}
}
