package com.epam.newsmanagement.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of
 * {@link com.epam.newsmanagement.dao.CommentDao} interface that using Hibernate
 * to achieve connectivity with database. Here we using
 * {@link org.hibernate.SessionFactory} object to get session and interact with
 * certain table in database.
 *
 */
@Transactional
public class HibernateCommentDaoImpl implements CommentDao {

	private static final String SQL_DELETE_COMMENT_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#add(Comment)} method using
	 * Hibernate.
	 */
	@Override
	public Long add(Comment comment) throws DaoException {
		try {
			sessionFactory.getCurrentSession().save(comment);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comment.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#delete(Long)} method using
	 * Hibernate
	 */
	@Override
	public void delete(Long commentId) throws DaoException {

		try {
			Comment comment = (Comment) sessionFactory.getCurrentSession().get(
					Comment.class, commentId);
			if (comment != null) {
				sessionFactory.getCurrentSession().delete(comment);
			}
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#deleteByNewsId(Long)}
	 * method using Hibernate.
	 */

	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					SQL_DELETE_COMMENT_BY_NEWS_ID);
			query.setLong(0, newsId);
			query.executeUpdate();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#update(Comment)} method
	 * using Hibernate.
	 */

	@Override
	public void update(Comment comment) throws DaoException {
		try {
			sessionFactory.getCurrentSession().update(comment);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#loadById(Long)} method
	 * using Hibernate.
	 */

	@Override
	public Comment loadById(Long commentId) throws DaoException {
		Comment comment = new Comment();
		try {
			comment = (Comment) sessionFactory.getCurrentSession().get(
					Comment.class, commentId);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comment;
	}
}
