package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * News class is representing news model for table NEWS in database. NEWS table
 * fields are
 * NEWS_ID;TITLE,SHORT_TEXT;FULL_TEXT;CREATION_DATE;MODIFICATION_DATE;
 */
@Entity
@Table(name = "NEWS")
@NamedQuery(name = "News.LoadAll", query = "SELECT n FROM News n")
@NamedNativeQueries({
		@NamedNativeQuery(name = "News.LoadIdsBySpecifiedPage", query = "select * from (select a.*, ROWNUM rnum from (SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC) a where rownum <= 3 + 3*?) where rnum >= 1 + 3*?"),
		@NamedNativeQuery(name = "News.LoadAllNewsId", query = "SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC"),
		@NamedNativeQuery(name = "News.LoadSingleNewsMessage", query = "SELECT NEWS.NEWS_ID,COMMENTS.COMMENT_ID,TAG.TAG_ID, AUTHOR.AUTHOR_ID FROM NEWS_TAG INNER JOIN NEWS  ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID  INNER JOIN TAG  ON TAG.TAG_ID = NEWS_TAG.TAG_ID  INNER JOIN NEWS_AUTHOR  ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID  INNER JOIN AUTHOR  ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID WHERE NEWS.NEWS_ID = ? GROUP BY NEWS.NEWS_ID,COMMENTS.COMMENT_ID,TAG.TAG_ID,AUTHOR.AUTHOR_ID"),
		@NamedNativeQuery(name = "News_Author.Add", query = "INSERT INTO news_author VALUES(?,?)"),
		@NamedNativeQuery(name = "News_Author.DeleteByNewsId", query = "DELETE FROM news_author WHERE news_id=?"),
		@NamedNativeQuery(name = "News_Author.DeleteByAuthorId", query = "DELETE FROM news_author WHERE author_id=?"),
		@NamedNativeQuery(name = "News_Tag.Add", query = "INSERT INTO news_tag VALUES(?,?)"),
		@NamedNativeQuery(name = "News_Tag.DeleteByTagId", query = "DELETE FROM news_tag WHERE tag_id=?"),
		@NamedNativeQuery(name = "News_Tag.DeleteByNewsId", query = "DELETE FROM news_tag WHERE news_id=?") })
public class News implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "NEWS_SEQUENCE", sequenceName = "NEWS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQUENCE")
	@Column(name = "NEWS_ID")
	private Long id;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "SHORT_TEXT")
	private String shortText;

	@Column(name = "FULL_TEXT")
	private String fullText;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	@Version
	@Column(name = "VERSION_OPT_LOCK")
	private Long version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * This method is using for string representation of News object.
	 */
	@Override
	public String toString() {
		return new StringBuilder("News [id=").append(id).append(", title=")
				.append(title).append(", shortText=").append(shortText)
				.append(", fullText=").append(fullText)
				.append(", creationDate=").append(creationDate)
				.append(", modificationDate=").append(modificationDate)
				.append(", Version = ").append(version).append("]").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
