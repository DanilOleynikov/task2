package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of {@link com.epam.newsmanagement.dao.AuthorDao}
 * interface, that using Hibernate technology for interactions with database; In
 * this class we using {@link org.hibernate.SessionFactory} object to achieve
 * connection and interaction with database;
 *
 */
@Transactional
public class HibernateAuthorDaoImpl implements AuthorDao {

	private static final String SQL_SELECT_ALL_NON_EXPIRED_AUTHORS = "SELECT * FROM author WHERE EXPIRED>SYSTIMESTAMP AND AUTHOR_ID NOT IN(SELECT AUTHOR_ID FROM NEWS_AUTHOR)";

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#updateAuthorExpired(Long authorId)}
	 * method using Hibernate.
	 */
	@Override
	public void updateAuthorExpired(Long authorId) throws DaoException {
		Author author = new Author();

		Author upAuthor = new Author();
		upAuthor = loadById(authorId);

		if (upAuthor != null) {
			try {
				author = (Author) sessionFactory.getCurrentSession().load(
						Author.class, authorId);
				author.setExpiredTime(new Date());
			} catch (HibernateException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}

		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#updateAuthorName(Long authorId, String name)}
	 * method using Hibernate.
	 */
	@Override
	public void updateAuthorName(Long authorId, String name)
			throws DaoException {
		Author author = new Author();
		Author upAuthor = new Author();
		upAuthor = loadById(authorId);

		if (upAuthor != null) {
			try {
				author = (Author) sessionFactory.getCurrentSession().load(
						Author.class, authorId);
				author.setName(name);
			} catch (HibernateException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#add(Author)} method using
	 * Hibernate.
	 */

	@Override
	public Long add(Author author) throws DaoException {
		try {
			sessionFactory.getCurrentSession().save(author);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return author.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#update(Author)} method using
	 * Hibernate.
	 */
	@Override
	public void update(Author author) throws DaoException {
		try {
			sessionFactory.getCurrentSession().update(author);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadById(Long)} method using
	 * Hibernate.
	 */

	@Override
	public Author loadById(Long authorId) throws DaoException {
		Author author = new Author();
		try {
			author = (Author) sessionFactory.getCurrentSession().get(
					Author.class, authorId);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		return author;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.AuthorDao#loadAll()}
	 * method using Hibernate.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Author> loadAll() throws DaoException {
		List<Author> authors = new ArrayList<Author>();

		try {
			authors = sessionFactory.getCurrentSession()
					.createCriteria(Author.class).list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		return authors;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadAllNonExpired()} method
	 * using Hibernate.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Author> loadAllNonExpired() throws DaoException {
		List<Author> nonExpiredAuthors = new ArrayList<Author>();
		try {
			Query query = sessionFactory.getCurrentSession()
					.createSQLQuery(SQL_SELECT_ALL_NON_EXPIRED_AUTHORS)
					.addEntity(Author.class);
			nonExpiredAuthors = query.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return nonExpiredAuthors;
	}
}
