package com.epam.newsmanagement.exception;

/**
 * 
 * All exceptions that may arise in SERVICE layer, are wrapped by this custom
 * exception.
 * 
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of ServiceException with a detail message.
	 * 
	 * @param message
	 *            the detail message of ServiceException
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Constructor of ServiceException with the parameter of root cause.
	 * 
	 * @param ex
	 *            parameter that consist of root cause.
	 */
	public ServiceException(Exception ex) {
		super(ex);
	}

	/**
	 * Constructor of ServiceException with a detail message of exception and
	 * root cause object.
	 * 
	 * @param message
	 *            the detail message of exception
	 * @param e
	 *            object of root cause of exception
	 */
	public ServiceException(String message, Exception ex) {
		super(message, ex);
	}

}
