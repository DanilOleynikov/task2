package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * NewsManagementService is an interface that are using for manipulating
 * database through such interfaces as CommentDao,NewsDao,TagDao,AuthorDao.
 */

public interface NewsManagmentService {

	/**
	 * This method is using for saving new NEWS object with author and list of
	 * tags;
	 * 
	 * @param news
	 *            {@link com.epam.newsmanagement.entity.News} object with
	 *            previous populated fields;
	 * @param authorId
	 *            ID (Long value) of news author;
	 * @param tagId
	 *            One ore more ID's of tag's (Long value) of news;
	 * 
	 * @return ID (Long value) of created news(ID value is getting from
	 *         database);
	 * @throws ServiceException
	 *             if one of the service methods failed(e.g newsService or
	 *             commentService methods);
	 */
	public Long saveNewsWithAuthorAndTags(News news, Long authorId,
			Long... tagId) throws ServiceException;

	/**
	 * This method is using for proper( e.g. cascade) deleting of news object
	 * from database. First it will delete all records in connective tables,
	 * than in tables that contain entity records.
	 * 
	 * @param newsId
	 *            ID of news that are going to be deleted.
	 * @throws ServiceException
	 *             if one of the service methods failed(e.g newsService or
	 *             commentService methods);
	 * 
	 */
	public void deleteNews(Long newsId) throws ServiceException;

}
