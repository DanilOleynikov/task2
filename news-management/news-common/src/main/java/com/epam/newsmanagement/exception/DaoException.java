package com.epam.newsmanagement.exception;

/**
 * 
 * All exceptions that may arise in DAO layer(e.g SQLException's), are wrapped
 * by this custom exception.
 * 
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of DAOException with the parameter of root cause.
	 * 
	 * @param ex
	 *            parameter that consist of root cause.
	 */
	public DaoException(Exception ex) {
		super(ex);
	}

	/**
	 * Constructor of DAOException with a detail message.
	 * 
	 * @param message
	 *            the detail message of DAOException
	 */
	public DaoException(String message) {
		super(message);
	}

	/**
	 * Constructor of DAOException with a detail message of exception and root
	 * cause object.
	 * 
	 * @param message
	 *            the detail message of exception
	 * @param e
	 *            object of root cause of exception
	 */
	public DaoException(String message, Exception e) {
		super(message, e);
	}
}
