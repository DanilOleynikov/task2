package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * TagDao is a common interface for interactions with table TAG in database. In
 * order to interact with this table, implement this interface and write you
 * realization for particular database;
 */

public interface TagDao {
	/**
	 * This method is using for creating new TAG object in database.
	 * 
	 * @param tag
	 *            TAG object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created TAG,
	 *         otherwise 0;
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public Long add(Tag tag) throws DaoException;

	/**
	 * This method is using for deleting TAG object(row) from database by ID of
	 * TAG.
	 * 
	 * @param tagId
	 *            ID of TAG that are going to be deleted.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void delete(Long tagId) throws DaoException;

	/**
	 * This method is using for updating TAG object in database.
	 * 
	 * @param tag
	 *            TAG object that contains all filled fields.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void update(Tag tag) throws DaoException;

	/**
	 * This method is using for loading the TAG object(row) from database if it
	 * exists.
	 * 
	 * @param tagId
	 *            ID of searching TAG.
	 * @return tag object with a specified TAG ID. If it doesn't exists, returns
	 *         not null TAG object, with null fields.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public Tag loadById(Long tagId) throws DaoException;

	/**
	 * This method is using for loading all tags from table TAG from database;
	 * 
	 * @return list of existing tags in database;
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public List<Tag> loadAll() throws DaoException;

}
