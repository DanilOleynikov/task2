package com.epam.newsmanagement.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of
 * {@link com.epam.newsmanagement.dao.CommentDao} interface, that allow to
 * interact with COMMENTS table in database using JPA with EclipseLink as
 * persistence provider.
 */
@Transactional
public class JpaCommentDaoImpl implements CommentDao {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#add(Comment)} method using
	 * JPA(EclipseLink).
	 */
	@Override
	public Long add(Comment comment) throws DaoException {
		try {
			em.persist(comment);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comment.getId();
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#delete(Long)} method using
	 * JPA(EclipseLink).
	 */

	@Override
	public void delete(Long commentId) throws DaoException {
		Comment comment;
		comment = loadById(commentId);
		if (comment != null) {
			try {
				em.remove(comment);
			} catch (PersistenceException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#deleteByNewsId(Long)}
	 * method using JPA(EclipseLink).
	 */
	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		try {
			em.createNamedQuery("Comment.deleteByNewsId")
					.setParameter("news_id", newsId).executeUpdate();
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#update(Comment)} method
	 * using JPA(EclipseLink).
	 */

	@Override
	public void update(Comment comment) throws DaoException {
		try {
			em.merge(comment);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#loadById(Long)} method
	 * using JPA(EclipseLink).
	 */

	@Override
	public Comment loadById(Long commentId) throws DaoException {
		Comment comment = new Comment();
		try {
			comment = em.find(Comment.class, commentId);
		} catch (PersistenceException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comment;
	}

}
