package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is implementation of {@link com.epam.newsmanagement.dao.TagDao}
 * interface using Hibernate. To achieve necessary functionality it's using
 * {@link org.hibernate.SessionFactory} object, that allow us make session and
 * retrieve information form database.
 */
@Transactional
public class HibernateTagDaoImpl implements TagDao {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#add(Tag)}
	 * method using Hibernate.
	 */
	@Override
	public Long add(Tag tag) throws DaoException {
		try {
			sessionFactory.getCurrentSession().save(tag);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tag.getId();
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#delete(Long)}
	 * method using Hibernate.
	 */

	@Override
	public void delete(Long tagId) throws DaoException {

		Tag tag = loadById(tagId);
		if (tag != null) {
			try {
				sessionFactory.getCurrentSession().delete(tag);
			} catch (HibernateException ex) {
				throw new DaoException(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#update(Tag)}
	 * method using Hibernate.
	 */

	@Override
	public void update(Tag tag) throws DaoException {
		try {
			sessionFactory.getCurrentSession().update(tag);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.TagDao#loadById(Long)} method using
	 * Hibernate.
	 */

	@Override
	public Tag loadById(Long tagId) throws DaoException {
		Tag tag = new Tag();
		try {
			tag = (Tag) sessionFactory.getCurrentSession()
					.get(Tag.class, tagId);
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tag;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#loadAll()}
	 * method using Hibernate.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> loadAll() throws DaoException {
		List<Tag> tags = new ArrayList<Tag>();
		try {
			tags = sessionFactory.getCurrentSession().createCriteria(Tag.class)
					.list();
		} catch (HibernateException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tags;
	}

}
