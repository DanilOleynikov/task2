package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

/**
 * CommentDao is a common interface for interactions with table COMMENTS in
 * database. In order to interact with this table, implement this interface and
 * write you realization for particular database;
 */
public interface CommentDao {

	/**
	 * This method is using for creating new COMMENT object in database.
	 *
	 * @param comment
	 *            COMMENT object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created
	 *         COMMENT, otherwise 0;
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public Long add(Comment comment) throws DaoException;

	/**
	 * This method is using for deleting COMMENT object(row) from database by ID
	 * of COMMENT.
	 * 
	 * @param commentId
	 *            ID of COMMENT that are going to be deleted.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void delete(Long commentId) throws DaoException;

	/**
	 * This method is using for deleting COMMENT object by NEWS ID.
	 * 
	 * @param newsId
	 *            ID of NEWS with whom this comment is connected.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void deleteByNewsId(Long newsId) throws DaoException;

	/**
	 * This method is using for updating COMMENT object in database.
	 * 
	 * @param comment
	 *            COMMENT object that contains all filled fields.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void update(Comment comment) throws DaoException;

	/**
	 * This method is using for loading the COMMENT object(row) from database if
	 * it exists
	 * 
	 * @param commentId
	 *            ID of searching COMMENT.
	 * @return COMMENT object with a specified COMMENT ID. If it doesn't exists,
	 *         returns not null COMMENT object, with null fields.
	 * @throws DaoException
	 *             this exception throws when on dao layer (e.g.) SQLException
	 *             caught.
	 */
	public Comment loadById(Long commentId) throws DaoException;

}
