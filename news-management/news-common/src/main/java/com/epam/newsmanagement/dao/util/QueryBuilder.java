package com.epam.newsmanagement.dao.util;

import com.epam.newsmanagement.entity.SearchCriteria;

/**
 * QueryBuilder class is using for creating different queries for database that
 * are using in DAO layer(methods)
 * 
 */
public class QueryBuilder {

	// //Parts for building query from SearchCriteria object/////////////////

	private static final String SQL_MAIN_PART = "select * from (select a.*, ROWNUM rnum from (SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS_TAG INNER JOIN NEWS ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID INNER JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID INNER JOIN AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID WHERE ";
	private static final String SQL_AUTHOR_PART = "AUTHOR.AUTHOR_ID = ";
	private static final String SQL_TAG_FIRST_PART = "TAG.TAG_ID IN";
	private static final String SQL_TAG_SECOND_PART = " HAVING COUNT(DISTINCT TAG.TAG_ID) = ";
	private static final String SQL_LEFT_BRACKET = "(";
	private static final String SQL_RIGHT_BRACKET = ")";
	private static final String SQL_COMMA = ",";
	private static final String SQL_GROUP_BY_CLAUSE = " GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE";
	private static final String SQL_ORDER_BY_CLAUSE = " ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC)";
	private static final String SQL_ROWNUM_PART = "a where rownum <= 3 + 3*?) where rnum >= 1 + 3*?";
	private static final String SQL_AND_PART = " AND ";
	private static final String SQL_WHERE_PART = " WHERE ";

	// //Parts for building query for inserting links into NEWS_TAG table///

	private static final String SQL_INSERT_ALL_PART = "INSERT ALL";
	private static final String SQL_INTO_PLUS_VALUES_PART = " INTO NEWS_TAG(NEWS_ID,TAG_ID) VALUES(?,?)";
	private static final String SQL_DUAL_PART = " SELECT * FROM DUAL";

	/**
	 * This method is using for constructing SQL queries according to the passed
	 * SearchCriteria object with specified id of author or/and tags. If user
	 * passed SearchCriteria object with filled only tagId list of authorId,
	 * this method construct query that will search only by tagId or authorId.
	 * If user passed both parameters it will build query according to both
	 * parameters.
	 * 
	 * @param sc
	 *            Search criteria object that contains filled authorId or/and
	 *            tagId objects(lists).
	 * @return SQL query in String representation.
	 */
	public static String buildQueryFromSearchCriteria(SearchCriteria sc) {

		StringBuilder resultQuery = new StringBuilder();

		if (sc != null) {
			if (sc.getAuthorId() != null || sc.getTagId() != null) {
				resultQuery.append(SQL_MAIN_PART);
				if (sc.getAuthorId() != null) {
					resultQuery.append(SQL_AUTHOR_PART);
					resultQuery.append(sc.getAuthorId());

				}

				if (sc.getTagId() != null) {
					if (sc.getAuthorId() != null) {
						resultQuery.append(SQL_AND_PART);
					}

					resultQuery.append(SQL_TAG_FIRST_PART).append(
							SQL_LEFT_BRACKET);
					for (Long l : sc.getTagId()) {
						resultQuery.append(l);
						resultQuery.append(SQL_COMMA);
					}

					resultQuery.deleteCharAt(resultQuery.length() - 1);
					resultQuery.append(SQL_RIGHT_BRACKET);
					resultQuery.append(SQL_TAG_SECOND_PART);
					resultQuery.append(sc.getTagId().length);

				}

				resultQuery.append(SQL_GROUP_BY_CLAUSE);
				resultQuery.append(SQL_ORDER_BY_CLAUSE);
				resultQuery.append(SQL_ROWNUM_PART);

			}
		}

		return resultQuery.toString();
	}

	public static String buildQueryForInsertingManyValuesInNewsTagAtOnce(
			Long newsId, Long... tagId) {
		StringBuilder resultQuery = new StringBuilder();

		if (newsId != null && tagId != null) {
			resultQuery.append(SQL_INSERT_ALL_PART);
			for (int i = 0; i < tagId.length; i++) {
				resultQuery.append(SQL_INTO_PLUS_VALUES_PART);
			}
			resultQuery.append(SQL_DUAL_PART);
		}

		return resultQuery.toString();

	}
}
