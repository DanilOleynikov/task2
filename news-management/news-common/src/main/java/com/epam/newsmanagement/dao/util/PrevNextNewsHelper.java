package com.epam.newsmanagement.dao.util;

import java.util.List;

import com.epam.newsmanagement.exception.ServiceException;

public class PrevNextNewsHelper {

	public Long getNextNewsId(Long newsId, List<Long> listOfNewsId)
			throws ServiceException {
		Long nextNewsId = new Long(0);
		if (!listOfNewsId.isEmpty() && newsId != null) {
			for (int i = 0; i < listOfNewsId.size() - 1; i++) {
				if (listOfNewsId.get(i) == newsId && i != listOfNewsId.size()) {
					nextNewsId = listOfNewsId.get(i + 1);
				}
			}
		}

		return nextNewsId;
	}

	public Long getPrevNewsId(Long newsId, List<Long> listOfNewsId)
			throws ServiceException {
		Long prevNewsId = new Long(0);

		if (!listOfNewsId.isEmpty() && newsId != null) {
			for (int i = 0; i < listOfNewsId.size(); i++) {
				if (listOfNewsId.get(i) == newsId && i != 0) {
					prevNewsId = listOfNewsId.get(i - 1);
				}
			}
		}

		return prevNewsId;
	}
}
