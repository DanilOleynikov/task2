package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.util.DatabaseUtil;
import com.epam.newsmanagement.dao.util.DateHelper;
import com.epam.newsmanagement.dao.util.QueryBuilder;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JdbcNewsDaoImpl class is a specific realization of
 * {@link com.epam.newsmanagement.dao.NewsDao} interface. According to this
 * class you can interact with database through simple JDBC.
 */
public class JdbcNewsDaoImpl implements NewsDao {

	private static final String SQL_CREATE_NEWS = "INSERT INTO news VALUES(news_seq.NEXTVAL,?,?,?,?,?,1)";
	private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id = ?";
	private static final String SQL_UPDATE_NEWS = "UPDATE news SET title = ?, short_text = ?, full_text = ?, creation_date = ?, modification_date = ?, version_opt_lock = version_opt_lock + 1 WHERE news_id = ? AND version_opt_lock = ?";
	private static final String SQL_FIND_NEWS = "SELECT * FROM news WHERE news_id = ?";
	private static final String SQL_GET_NEWS_COUNT = "SELECT COUNT(*) FROM NEWS";
	private static final String SQL_LOAD_IDS_OF_NEWS_ON_SPECIFIC_PAGE = "select * from (select a.*, ROWNUM rnum from (SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC) a where rownum <= 3 + 3*?) where rnum >= 1 + 3*?";

	private static final String SQL_LOAD_LIST_OF_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE = "SELECT NEWS.NEWS_ID,NEWS.CREATION_DATE,NEWS.FULL_TEXT,NEWS.MODIFICATION_DATE,NEWS.SHORT_TEXT,NEWS.TITLE,COUNT(COMMENTS.COMMENT_ID) as comCounter FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.CREATION_DATE,NEWS.FULL_TEXT,NEWS.MODIFICATION_DATE,NEWS.SHORT_TEXT,NEWS.TITLE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_LOAD_SINGLE_NEWS_MESSAGE = "SELECT  NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE, COMMENTS.COMMENT_TEXT,COMMENTS.CREATION_DATE as COMMENT_CREATION_DATE,COMMENTS.COMMENT_ID, TAG.TAG_NAME,TAG.TAG_ID, AUTHOR.AUTHOR_ID,AUTHOR.AUTHOR_NAME,AUTHOR.EXPIRED FROM NEWS_TAG INNER JOIN NEWS  ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID  INNER JOIN TAG  ON TAG.TAG_ID = NEWS_TAG.TAG_ID  INNER JOIN NEWS_AUTHOR  ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID  INNER JOIN AUTHOR  ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID WHERE NEWS.NEWS_ID = ? GROUP BY NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE, COMMENTS.COMMENT_TEXT,COMMENTS.CREATION_DATE,COMMENTS.COMMENT_ID,COMMENTS.NEWS_ID, TAG.TAG_NAME,TAG.TAG_ID,AUTHOR.AUTHOR_ID,AUTHOR.AUTHOR_NAME,AUTHOR.EXPIRED";

	private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO news_author VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "DELETE FROM news_author WHERE news_id=?";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID = "DELETE FROM news_author WHERE author_id=?";

	private static final String SQL_ADD_NEWS_TAG = "INSERT INTO news_tag VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "DELETE FROM news_tag WHERE tag_id=?";
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "DELETE FROM news_tag WHERE news_id=?";

	private static final String SQL_LOAD_ALL_NEWS_ID = "SELECT NEWS.NEWS_ID,COUNT(COMMENTS.COMMENT_ID) as comCounter,NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.MODIFICATION_DATE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC";
	private DataSource dataSource;

	/**
	 * This method is using for injecting(DI) datasource via Spring.
	 * 
	 * @param dataSource
	 *            Some datasource that already configured for concrete
	 *            database(e.g for oracle url/pass/login/driver).In this
	 *            application this setter is using for spring DI of DataSource.
	 *            Configuration for oracle database you can see in
	 *            database-config.properties file.
	 */

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadAllNewsId} method through
	 * simple JDBC.
	 */
	@Override
	public List<Long> loadAllNewsId() throws DaoException {
		List<Long> newsId = new LinkedList<Long>();

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_ALL_NEWS_ID);
			rs = pst.executeQuery();
			while (rs.next()) {
				newsId.add(rs.getLong("NEWS_ID"));
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdSearchedByCriteria}
	 * method through simple JDBC.
	 */
	public List<Long> loadNewsIdSearchedByCriteria(SearchCriteria sc, Long page)
			throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		String criteriaQuery = QueryBuilder.buildQueryFromSearchCriteria(sc);
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(criteriaQuery);
			pst.setLong(1, page);
			pst.setLong(2, page);

			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					newsId.add(rs.getLong("NEWS_ID"));
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);

		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadListOfNewsVoByNewsId}
	 * method through simple JDBC.
	 */
	@Override
	public List<NewsVO> loadListOfNewsVoByNewsId(List<Long> newsId)
			throws DaoException {

		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		for (Long l : newsId) {
			NewsVO newsVo = new NewsVO();
			newsVo = loadNewsVo(l);
			newsVoList.add(newsVo);
		}

		return newsVoList;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate}
	 * method through simple JDBC.
	 */
	public List<Long> loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(
			Long pageNumber) throws DaoException {

		List<Long> newsId = new ArrayList<Long>();
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_IDS_OF_NEWS_ON_SPECIFIC_PAGE);
			pst.setLong(1, pageNumber);
			pst.setLong(2, pageNumber);
			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					newsId.add(rs.getLong("NEWS_ID"));
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#getNewsCount} method through
	 * simple JDBC.
	 */
	@Override
	public Long getNewsCount() throws DaoException {
		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;
		Long newsCount = new Long(0);

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			st = cn.createStatement();
			rs = st.executeQuery(SQL_GET_NEWS_COUNT);

			if (rs != null) {
				while (rs.next()) {
					newsCount = rs.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, st, rs,
					dataSource);
		}

		return newsCount;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 * method through simple JDBC.
	 */

	@Override
	public Long add(News news) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newsId = new Long(0);
		String[] pk = { "NEWS_ID" };

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_CREATE_NEWS, pk);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4,
					new Timestamp(news.getCreationDate().getTime()),
					DateHelper.getUtcCalendar());
			pst.setDate(5, new Date(news.getModificationDate().getTime()),
					DateHelper.getUtcCalendar());
			pst.executeUpdate();

			rs = pst.getGeneratedKeys();

			if (rs != null && rs.next()) {
				newsId = rs.getLong(1);
			}
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)} method through
	 * simple JDBC.
	 */

	@Override
	public void update(News news) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_UPDATE_NEWS);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4,
					new Timestamp(news.getCreationDate().getTime()),
					DateHelper.getUtcCalendar());
			pst.setDate(5, new Date(news.getModificationDate().getTime()),
					DateHelper.getUtcCalendar());
			pst.setLong(6, news.getId());
			pst.setLong(7, news.getVersion());
			int isUpdated = pst.executeUpdate();
			if (isUpdated == 0) {
				throw new DaoException(
						"News wasn't updated due to optimistic locking");
			}
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)} method through
	 * simple JDBC.
	 */

	@Override
	public void delete(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)} method through
	 * simple JDBC.
	 */

	@Override
	public News loadById(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		News news = new News();

		TimeZone tz = TimeZone.getDefault();
		String utcOffset = DateHelper.getUtcOffset(tz);
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(utcOffset));

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_FIND_NEWS);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();

			while (rs.next()) {

				news.setId(rs.getLong("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date", cal));
				news.setModificationDate(rs.getDate("modification_date", cal));
				news.setVersion(rs.getLong("version_opt_lock"));

			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return news;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)} method
	 * through simple JDBC.
	 */

	@Override
	public NewsVO loadNewsVo(Long newsId) throws DaoException {
		NewsVO newsVo = new NewsVO();
		Author author = new Author();
		News news = new News();
		Set<Comment> comments = new HashSet<Comment>();
		Set<Tag> tags = new HashSet<Tag>();

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		TimeZone tz = TimeZone.getDefault();
		String utcOffset = DateHelper.getUtcOffset(tz);
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(utcOffset));

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_SINGLE_NEWS_MESSAGE,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if (rs != null && rs.next()) {

				author.setExpiredTime(rs.getTimestamp("expired", cal));
				author.setId(rs.getLong("AUTHOR_ID"));
				author.setName(rs.getString("AUTHOR_NAME"));
				newsVo.setAuthor(author);

				news.setCreationDate(rs.getTimestamp("creation_date", cal));
				news.setFullText(rs.getString("full_text"));
				news.setId(rs.getLong("news_id"));
				news.setModificationDate(rs.getDate("modification_date", cal));
				news.setShortText(rs.getString("short_text"));
				news.setTitle(rs.getString("title"));
				newsVo.setNews(news);

			}
			comments = makeListOfCommentsFromResultSet(rs);
			tags = makeListOfTagsFromResultSet(rs);

			if (!comments.isEmpty()) {
				newsVo.setComments(comments);
			}

			newsVo.setTags(tags);

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return newsVo;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 * method through simple JDBC.
	 */

	@Override
	public List<News> loadAll() throws DaoException {

		List<News> sortedNews = new LinkedList<News>();
		Statement st = null;
		Connection cn = null;
		ResultSet rs = null;
		News news = null;

		TimeZone tz = TimeZone.getDefault();
		String utcOffset = DateHelper.getUtcOffset(tz);
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(utcOffset));

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			st = cn.createStatement();
			rs = st.executeQuery(SQL_LOAD_LIST_OF_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE);
			if (rs != null) {
				while (rs.next()) {

					news = new News();
					news.setId(rs.getLong("NEWS_ID"));
					news.setTitle(rs.getString("TITLE"));
					news.setShortText(rs.getString("SHORT_TEXT"));
					news.setFullText(rs.getString("FULL_TEXT"));
					news.setCreationDate(rs.getTimestamp("CREATION_DATE", cal));
					news.setModificationDate(rs.getDate("MODIFICATION_DATE",
							cal));
					sortedNews.add(news);
				}
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, st, rs,
					dataSource);
		}
		return sortedNews;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			pst.execute();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)
	 * )} method through simple JDBC.
	 */

	public void addNewsTagLink(Long newsId, Long... tagId) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			String query = QueryBuilder
					.buildQueryForInsertingManyValuesInNewsTagAtOnce(newsId,
							tagId);
			if (query.length() != 0) {
				pst = cn.prepareStatement(query);
				for (int i = 0; i < tagId.length; i++) {
					pst.setLong(2 * i + 1, newsId);
					pst.setLong(2 * i + 2, tagId[i]);
				}
			}
			pst.execute();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_ADD_NEWS_TAG);
			pst.setLong(1, newsId);
			pst.setLong(2, tagId);
			pst.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID);
			pst.setLong(1, authorId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsTagLink(Long tagId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_TAG_BY_TAG_ID);
			pst.setLong(1, tagId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * This method is using for retrieving data(Comment objects) from ResultSet.
	 * 
	 * @param rs
	 *            ResultSet object with necessary data(records of comments)
	 * @return Set of comments extracted from ResultSet.
	 * @throws DaoException
	 *             if any SQLException acquired on this layer
	 */
	private Set<Comment> makeListOfCommentsFromResultSet(ResultSet rs)
			throws DaoException {
		Set<Comment> comments = new HashSet<Comment>();

		TimeZone tz = TimeZone.getDefault();
		String utcOffset = DateHelper.getUtcOffset(tz);
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(utcOffset));

		try {
			rs.beforeFirst();

			while (rs.next()) {
				Comment comment = new Comment();

				comment.setCreationTime(rs.getTimestamp(
						"comment_creation_date", cal));
				comment.setId(rs.getLong("comment_id"));
				comment.setNewsId(rs.getLong("news_id"));
				comment.setText(rs.getString("comment_text"));
				comments.add(comment);
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comments;
	}

	/**
	 * This method is using for retrieving data(Tag objects) from ResultSet.
	 * 
	 * @param rs
	 *            ResultSet object with necessary data(records of tags)
	 * @return Set of tags extracted from ResultSet.
	 * @throws DaoException
	 *             if any SQLException acquired on this layer
	 */
	private Set<Tag> makeListOfTagsFromResultSet(ResultSet rs)
			throws DaoException {
		Set<Tag> tags = new HashSet<Tag>();
		try {

			rs.beforeFirst();
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tags;
	}

}
