package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * CommentService is a common interface for service layer, implementation of
 * which is using for operating COMMENTS table in database;
 */

public interface CommentService {

	/**
	 * This method is using for creating new COMMENT object in database.
	 *
	 * @param comment
	 *            COMMENT object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created
	 *         COMMENT, otherwise 0;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public Long add(Comment comment) throws ServiceException;

	/**
	 * This method is using for deleting COMMENT object(row) from database by ID
	 * of COMMENT.
	 * 
	 * @param commentId
	 *            ID of COMMENT that are going to be deleted.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void delete(Long commentId) throws ServiceException;

	/**
	 * This method is using for deleting COMMENT object by NEWS ID.
	 * 
	 * @param newsId
	 *            ID of NEWS with whom this comment is connected.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void deleteByNewsId(Long newsId) throws ServiceException;

	/**
	 * This method is using for updating COMMENT object in database.
	 * 
	 * @param comment
	 *            COMMENT object that contains all filled fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void update(Comment comment) throws ServiceException;

	/**
	 * This method is using for loading the COMMENT object(row) from database if
	 * it exists
	 * 
	 * @param commentId
	 *            ID of searching COMMENT.
	 * @return COMMENT object with a specified COMMENT ID. If it doesn't exists,
	 *         returns not null COMMENT object, with null fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public Comment loadById(Long commentId) throws ServiceException;

}
