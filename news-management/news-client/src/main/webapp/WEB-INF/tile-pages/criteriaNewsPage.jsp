<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<a href='<c:url value="/Controller?command=NEWSLISTPAGE&page=1"></c:url>'><fmt:message key="label.search-criteria.back" bundle="${rb}" /></a>
	<c:if test="${empty criteriaNewsVoList}">
		<fmt:message key="label.search-criteria.no-news-found-info" bundle="${rb}" />
	</c:if>
		<c:if test="${not empty criteriaNewsVoList}">
			<c:forEach var="newsVo" items="${criteriaNewsVoList}">

				<div class="news">
					<div id="news-title">
						<span
							class="newstitledec">${newsVo.news.title}</span>
					</div>
					<div id="news-author">by(${newsVo.author.name})</div>
					<div id="news-date">${newsVo.news.modificationDate}</div>


					<div id="news-shorttext">${newsVo.news.shortText}</div>

					<c:set var="i" value="1"></c:set>
					<c:set var="tagsListSize" value="${fn:length(newsVo.tags)}" />

					<div id="info-news">
						<ul>

							<li class="li" id="tags"><span class="tagsdec"> <c:forEach
										var="tag" items="${newsVo.tags}">

										<c:choose>
											<c:when test="${i != tagsListSize}">
											${tag.name},
											<c:set var="i" value="${i+1}" />
											</c:when>
											<c:otherwise>
											${tag.name}
										</c:otherwise>
										</c:choose>
									</c:forEach>
							</span></li>
							<li class="li" id="comments"><span class="commentsdec"><fmt:message key="label.search-criteria.comments" bundle="${rb}" />(${fn:length(newsVo.comments)})</span></li>
							<li class="li"><a href='<c:url value="/Controller?command=VIEWNEWS&id=${newsVo.news.id}"></c:url>'><span
							class="newstitledec"><fmt:message key="label.search-criteria.view" bundle="${rb}" /></span></a></li>
						</ul>
					</div>
				</div>
			</c:forEach>
		</c:if>
</body>
</html>