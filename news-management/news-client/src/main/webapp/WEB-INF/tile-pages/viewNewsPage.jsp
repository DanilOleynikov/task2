<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources.pagecontent" var="rb" />
<%@ page import="java.util.Date"%>
<jsp:useBean id="current" class="java.util.Date" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${newsVo.news.title}</title>
</head>

<a
	href='<c:url value="/Controller?command=NEWSLISTPAGE&page=1"></c:url>'><fmt:message
		key="label.view-news.back" bundle="${rb}" /></a>
<c:set var="token">${current.time}</c:set>
<c:if test="${empty sessionScope.token}">
	<c:set var="token" value="${token}" scope="session"></c:set>
</c:if>
<c:choose>
	<c:when test="${newsVo.news.id gt 0}">
		<div class="news">
			<div id="news-title">${newsVo.news.title}</div>
			<div id="news-author">by(${newsVo.author.name})</div>
			<div id="news-date">${newsVo.news.modificationDate}</div>
			<div id="news-fulltext">${newsVo.news.fullText}</div>

		</div>
		<c:forEach var="comment" items="${newsVo.comments}">
			<c:if test="${comment.id ne 0}">
				<div id="comment-block">
					<span style="text-decoration: underline";padding-top:20px;>${comment.creationTime}</span>
					<div id="comment-content">
						<div style="margin: 0 auto; text-align: right;"></div>
						${comment.text}

					</div>
				</div>
			</c:if>
		</c:forEach>

		<div id="add-comment-box">
			<form
				action="${pageContext.request.contextPath}/Controller?command=ADDCOMMENT"
				method="POST">
				<textarea style="width: 200px" name="commentText" required
					oninput="InvalidMsg(this)" oninvalid="InvalidMsg(this)"
					placeholder="<fmt:message key="label.view-news.text-comment-placeholder" bundle="${rb}" />"></textarea>
				<input type="hidden" value="${token}" name="token" /> <input
					type="hidden" name="newsId" value="${newsVo.news.id}" /> <input
					type="submit"
					value="<fmt:message key="label.view-news.button.post" bundle="${rb}" />"></input>
			</form>
		</div>
		<c:if test="${previousNewsId!=0}">
			<a
				href="<c:url value="/Controller?command=VIEWNEWS&id=${previousNewsId}"></c:url>">Previous</a>
		</c:if>
		<c:if test="${nextNewsId!=0}">
			<a
				href="<c:url value="/Controller?command=VIEWNEWS&id=${nextNewsId}"></c:url>">Next</a>
		</c:if>
		${NotValidComment}
	</c:when>

	<c:otherwise>
		<fmt:message key="label.view-news.no-news-found-info" bundle="${rb}" />
	</c:otherwise>
</c:choose>
<c:if test="${!empty doubleSubmissionWarning}">
	${doubleSubmissionWarning}
</c:if>

</body>
</html>