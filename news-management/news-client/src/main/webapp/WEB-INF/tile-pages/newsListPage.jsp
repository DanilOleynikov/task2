<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<fmt:setBundle basename="resources.pagecontent" var="rb" />
<link href="<c:url value="/resources/css/templateStyle.css" />"
	rel="stylesheet">
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


 <c:set var="selectTags"><fmt:message key="label.select-tags" bundle="${rb}"/></c:set>
 
<script type="text/javascript">
	$(function() {
		$("#lstTags").dropdownchecklist({
			icon : {},
			emptyText : "${selectTags}",
			width : 150
		});
		$("#lstAuthors").dropdownchecklist({
			icon : {},
			width : 160
		});
	});
</script>
<title><fmt:message key="label.news-list-page.title" bundle="${rb}" /></title>
</head>
<body>
	<div class="search">

		<form action='<c:url value="/Controller?command=SEARCHNEWS"></c:url>' method="POST">

			<select name=authorId class="authorselect" id="lstAuthors">
				<option selected="selected" disabled><fmt:message key="label.select-authors" bundle="${rb}" /></option>
				<c:forEach var="author" items="${authorList}">
					<option value="${author.id}">${author.name}</option>
				</c:forEach>
			</select> <select name="tagId" class="tagselect" id="lstTags"
				multiple="multiple">
				<c:forEach var="tag" items="${tagList}">
					<option value="${tag.id}">${tag.name}</option>
				</c:forEach>

			</select> <input type="submit" value="<fmt:message key="label.news-list.button.filter" bundle="${rb}" />" class="findbutton" /> <input
				type="submit" value="<fmt:message key="label.news-list.button.reset" bundle="${rb}" />" class="resetbutton" />
		</form>

	</div>

	<c:if test="${empty newsVoList}">
	<fmt:message key="label.news-list.no-news-info" bundle="${rb}" />
	
	</c:if>

	<c:if test="${not empty newsVoList}">
		<c:forEach var="newsVo" items="${newsVoList}">

			<div class="news">
				<div id="news-title">
					<span class="newstitledec">${newsVo.news.title}</span>
				</div>
				<div id="news-author">by(${newsVo.author.name})</div>
				<div id="news-date">${newsVo.news.modificationDate}</div>


				<div id="news-shorttext">${newsVo.news.shortText}</div>

				<c:set var="i" value="1"></c:set>
				<c:set var="tagsListSize" value="${fn:length(newsVo.tags)}" />

				<div id="info-news">
					<ul>

						<li class="li" id="tags"><span class="tagsdec"> <c:forEach
									var="tag" items="${newsVo.tags}">

									<c:choose>
										<c:when test="${i != tagsListSize}">
											${tag.name},
											<c:set var="i" value="${i+1}" />
										</c:when>
										<c:otherwise>
											${tag.name}
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</span></li>

						<c:set var="realCommentaryCounter" value="0"></c:set>
						<c:forEach var="comment" items="${newsVo.comments}">
							<c:if test="${comment.id ne 0}">
								<c:set var="realCommentaryCounter"
									value="${realCommentaryCounter+1}"></c:set>
							</c:if>
						</c:forEach>
						
						<li class="li" id="comments"><span class="commentsdec"><fmt:message key="label.news-list.comments" bundle="${rb}" />(${realCommentaryCounter})</span></li>
						<li class="li"><span><a href="#"><a href='<c:url value="/Controller?command=VIEWNEWS&id=${newsVo.news.id}"></c:url>'><span
						class="newstitledec"><fmt:message key="label.news-list.view" bundle="${rb}" /></span></a></a></span></li>
					</ul>
				</div>
			</div>
		</c:forEach>
	</c:if>

	<div id="paging">
		<table border="1" cellpadding="5" cellspacing="5">
			<tr>
				<c:if test="${currentPage != 1}">
					<td><a
						href="<c:url value="/Controller?command=NEWSLISTPAGE&page=${currentPage-1}"></c:url>"><fmt:message key="label.news-list.previous" bundle="${rb}" /></a></td>
				</c:if>

				<c:forEach begin="1" end="${numberOfPages}" var="i">
					<c:choose>
						<c:when test="${currentPage eq i}">
							<td>${i}</td>
						</c:when>
						<c:otherwise>
							<td><a href="<c:url value='/Controller?command=NEWSLISTPAGE&page=${i}' />">${i}</a></td>
						</c:otherwise>
					</c:choose>
				</c:forEach>

				<c:if test="${currentPage lt numberOfPages}">
					<td><a
						href="<c:url value='/Controller?command=NEWSLISTPAGE&page=${currentPage + 1}' />"><fmt:message key="label.news-list.next" bundle="${rb}" /></a></td>

				</c:if>
			</tr>
		</table>
	</div>

</body>
</html>