<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<fmt:setLocale value="${lang}" scope="session" />

<fmt:setBundle basename="resources.pagecontent" var="rb" />
<div id="header-info">
	<H1>
		<fmt:message key="label.header.title" bundle="${rb}" />
	</H1>
</div>

<div id="language-selector">
<a href="<c:url value='/Controller?command=CHANGELANGUAGE&language=en_US'/>">EN</a> <a href="<c:url value='/Controller?command=CHANGELANGUAGE&language=ru_RU'/>">RU</a>

</div>







