<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id='cssmenu'>
	<ul>
		<li class='active'><a href="<c:url value='/newsListPage/1' />" ><span>NewsList</span></a></li>
		<li><a href="<c:url value='/addNewsPage' />"><span>Add News</span></a></li>
		<li><a href="<c:url value='/manageAuthorsPage' />"><span>Add/Update Author</span></a></li>
		<li class='last'><a href="<c:url value='/manageTagsPage' />"><span>Add/Update Tag</span></a></li>
	</ul>
</div>
