<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<p>Copyright @ Epam 2015.<fmt:message key="label.footer.info" bundle="${rb}" /></p>

