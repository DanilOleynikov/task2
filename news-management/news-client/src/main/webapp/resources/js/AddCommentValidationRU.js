function InvalidMsg(textbox) {

		if (textbox.value == '') {
			textbox.setCustomValidity('Поле не может быть пустым!');
			
		} else if (textbox.value.length < 10) {
			textbox
					.setCustomValidity('Длина комментария должна быть не менее 10 символов!');
		} else if (textbox.value.length > 100) {
			textbox
					.setCustomValidity('Длина комментария не может превышать 100 символов!');
		} else {
			textbox.setCustomValidity('');
		}
		return true;
	}