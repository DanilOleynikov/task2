function InvalidMsg(textbox) {

		if (textbox.value == '') {
			textbox.setCustomValidity('The field cant be empty!');
			
		} else if (textbox.value.length < 10) {
			textbox
					.setCustomValidity('Length of comment must be not less than 10 symbols!');
		} else if (textbox.value.length > 100) {
			textbox
					.setCustomValidity('Length of comment must be not larger than 100 symbols!');
		} else {
			textbox.setCustomValidity('');
		}
		return true;
	}