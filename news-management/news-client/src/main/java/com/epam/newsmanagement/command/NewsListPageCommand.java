package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ControllerException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.PaginationHelper;

public class NewsListPageCommand implements ActionCommand {

	public static final String NEWS_LIST_PAGE = "/WEB-INF/pages/newsListPage.jsp";

	/**
	 * This method is using for processing NewsListPage. It will fill request
	 * object with all necessary data(such as
	 * NewsList,AuthorList,TagList,Pagination);
	 */
	@Override
	public String execute(HttpServletRequest request, ApplicationContext ac)
			throws ControllerException {

		NewsService newsService = (NewsService) ac.getBean("newsService");
		AuthorService authorService = (AuthorService) ac
				.getBean("authorService");
		TagService tagService = (TagService) ac.getBean("tagService");

		List<Author> authorList = new ArrayList<Author>();
		List<Tag> tagList = new ArrayList<Tag>();
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		List<Long> newsIdList = new ArrayList<Long>();

		Long newsCount = new Long(0);
		Long numberOfPages = new Long(0);

		Long currentPage = Long.valueOf(request.getParameter("page"));

		try {
			newsCount = newsService.getNewsCount();
			authorList = authorService.loadAll();
			tagList = tagService.loadAll();
			newsIdList = newsService
					.loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(currentPage - 1);
			newsVoList = newsService.loadListOfNewsVoByNewsId(newsIdList);
		} catch (ServiceException e) {
			throw new ControllerException(e.getMessage(), e);
		}

		if (newsCount != 0) {
			numberOfPages = PaginationHelper.getNumberOfPages(newsCount);
		}

		request.setAttribute("numberOfPages", numberOfPages);
		request.setAttribute("currentPage", currentPage);
		request.setAttribute("newsVoList", newsVoList);
		request.setAttribute("authorList", authorList);
		request.setAttribute("tagList", tagList);

		return NEWS_LIST_PAGE;
	}
}
