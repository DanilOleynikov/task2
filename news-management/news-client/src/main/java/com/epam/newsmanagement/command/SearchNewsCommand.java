package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ControllerException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.Converter;

public class SearchNewsCommand implements ActionCommand {

	private static final String CRITERA_NEWS_PAGE = "/WEB-INF/pages/criteriaNewsPage.jsp";

	/**
	 * This method is using for providing search functionality by mentioned
	 * tag(s) and author, or only tag(s)/author. Request object must contain
	 * necessary data for creating SearchCriteria object(such as tag(s)/author);
	 * If any news found, it will fill it into request object.
	 */
	@Override
	public String execute(HttpServletRequest request, ApplicationContext ac)
			throws ControllerException {

		NewsService newsService = (NewsService) ac.getBean("newsService");
		SearchCriteria sc = new SearchCriteria();

		String authorId = request.getParameter("authorId");
		String[] tagId = request.getParameterValues("tagId");

		if (tagId != null) {
			Long[] resultTagId = Converter.convertStringToLongArray(tagId);
			sc.setTagId(resultTagId);
		}
		if (authorId != null) {
			Long resultAuthorId = Long.valueOf(authorId);
			sc.setAuthorId(resultAuthorId);
		}

		List<NewsVO> criteriaNewsVoList = new ArrayList<NewsVO>();
		List<Long> newsIdList = new ArrayList<Long>();

		try {
			newsIdList = newsService.loadNewsIdSearchedByCriteria(sc, new Long(
					0));

			criteriaNewsVoList = newsService
					.loadListOfNewsVoByNewsId(newsIdList);

		} catch (ServiceException e) {
			throw new ControllerException(e.getMessage(), e);
		}

		request.setAttribute("criteriaNewsVoList", criteriaNewsVoList);

		return CRITERA_NEWS_PAGE;
	}
}
