package com.epam.newsmanagement.configmanager;

import java.util.ResourceBundle;

/**
 * This class is providing functionality to manipulate with resource file that
 * contain Russian labels.
 *
 */
public class ConfigurationRuMessageManager {

	private static final ResourceBundle bundle = ResourceBundle
			.getBundle("resources/pagecontent_ru_RU");

	private ConfigurationRuMessageManager() {
	};

	/**
	 * THis method retreaving message from resource file by key.
	 * 
	 * @param key
	 *            String value of marker in resource file.
	 * @return value of passed key in resource file.
	 */

	public static String getString(String key) {

		return bundle.getString(key);
	}
}
