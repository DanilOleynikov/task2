package com.epam.newsmanagement.command;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ControllerException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.util.CheckLocale;
import com.epam.newsmanagement.validator.CommentFormValidator;

public class AddCommentCommand implements ActionCommand {

	/**
	 * This method is using for validating and adding new comment to the certain
	 * news. HttpServletRequest object must contain id of news, that need to be
	 * added a new comment due to forward after adding of comment. In this
	 * method also CommentText is validating for length.If validation fails, it
	 * will put error message into request.
	 */

	@Override
	public String execute(HttpServletRequest request, ApplicationContext ac)
			throws ControllerException {
		String page = "/Controller?command=VIEWNEWS&id="
				+ request.getParameter("newsId");
		CommentService commentService = (CommentService) ac
				.getBean("commentService");

		String requestToken = request.getParameter("token");
		HttpSession session = request.getSession();
		String sessionToken = (String) session.getAttribute("token");

		if (requestToken.equals(sessionToken)) {
			CheckLocale.setErrorMessage(request, "doubleSubmissionWarning",
					"label.info.double-submission");
		} else {

			Long newsId = Long.valueOf(request.getParameter("newsId"));
			String commentText = " ";
			commentText = request.getParameter("commentText");

			boolean isCommentValid = CommentFormValidator
					.isCommentValid(commentText);

			if (isCommentValid) {
				Comment comment = new Comment();
				comment.setText(commentText);
				comment.setCreationTime(new Date());
				comment.setNewsId(newsId);
				comment.setText(commentText);

				try {
					commentService.add(comment);
				} catch (ServiceException e) {
					throw new ControllerException(e.getMessage(), e);
				}
			} else {
				CheckLocale.setErrorMessage(request, "NotValidComment",
						"label.view-news.textarea.comment.warning.too-small");
			}
			session.setAttribute("token", requestToken);
		}

		return page;
	}

}
