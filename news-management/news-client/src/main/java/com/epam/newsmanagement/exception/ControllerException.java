package com.epam.newsmanagement.exception;

/**
 * All Dao and Service exceptions, will be wrapped into this Exception, and then
 * will be processed in Controller class.
 */
public class ControllerException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of ControllerException with the parameter of root cause.
	 * 
	 * @param ex
	 *            parameter that consist of root cause.
	 */
	public ControllerException(Exception ex) {
		super(ex);
	}

	/**
	 * Constructor of ControllerException with a detail message.
	 * 
	 * @param message
	 *            the detail message of DAOException.
	 */
	public ControllerException(String message) {
		super(message);
	}

	/**
	 * Constructor of ControllerException with a detail message of exception and
	 * root cause object.
	 * 
	 * @param message
	 *            the detail message of exception.
	 * @param e
	 *            object of root cause of exception.
	 */
	public ControllerException(String message, Exception e) {
		super(message, e);
	}
}
