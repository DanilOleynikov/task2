package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;

public class ChangeLanguageCommand implements ActionCommand {

	private static final String NEWS_LIST_PAGE = "/Controller?command=NEWSLISTPAGE&page=1";

	/**
	 * This method is using for fulfilling requests from client to change
	 * language in application. Request object must contain parameter language,
	 * with filled value as ru_RU or en_US(they are supported in application for
	 * now); Otherwise it will set English language in session(it's default in
	 * application).
	 */
	@Override
	public String execute(HttpServletRequest request, ApplicationContext ac) {

		String language = request.getParameter("language");
		if (language != null) {
			request.getSession().setAttribute("lang", language);
		}
		return NEWS_LIST_PAGE;
	}

}
