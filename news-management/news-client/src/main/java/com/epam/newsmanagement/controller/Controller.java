package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.exception.ControllerException;
import com.epam.newsmanagement.factory.ActionFactory;
import com.epam.newsmanagement.util.RedirectCommand;

/**
 * This is class is representing only 1 servlet in application. It's responsible
 * for executing all commands that coming from application. Logic of commands is
 * built around the ActionFactory and CommandEnum.
 *
 */
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static ApplicationContext ac = null;
	private static Logger logger;

	/**
	 * In init() method we initializing context of application and logger
	 * object, because we need only once to initialize them in application.
	 */
	public void init() throws ServletException {

		ac = WebApplicationContextUtils.getWebApplicationContext(this
				.getServletContext());
		logger = LoggerFactory.getLogger(Controller.class);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * This method is calling in doGet and doPost methods, due to not copy logic
	 * in both methods two times. It will calling methods to define and the
	 * executing(if command is not empty or null) commands, and forwarding to
	 * the incoming page. Otherwise it will redirect to index page.
	 * 
	 * @param request
	 *            HttpServletRequest object that contain parameter command that
	 *            is existing in CommandEnum.
	 * @param response
	 *            HttpServletResponse object.
	 * @throws ServletException
	 *             thrown if any ServletException appeared.
	 * @throws IOException
	 *             thrown if any IOException appeared.
	 */
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String page = null;
		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);

		try {
			page = command.execute(request, ac);
		} catch (ControllerException e) {
			logger.error(e.getMessage(), e);
		}

		if (RedirectCommand.getInstance().isNeedToRedirect(
				request.getParameter("command"))) {
			response.sendRedirect(request.getContextPath() + page);
		}

		else {
			RequestDispatcher disp = getServletContext().getRequestDispatcher(
					page);
			disp.forward(request, response);
		}
	}
}
