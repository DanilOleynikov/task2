package com.epam.newsmanagement.util;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.configmanager.ConfigurationEnMessageManager;
import com.epam.newsmanagement.configmanager.ConfigurationRuMessageManager;

/**
 * This is support class for working with locale and messages, that will appear
 * according to the locale.In application it using for setting certain messages
 * on validation.
 */
public class CheckLocale {

	/**
	 * This method is setting certain message into variable, that can be shown
	 * on the certain jsp. It will based on the set locale in session. If it
	 * will Russian, than it will take Russian message, otherwise - English.
	 * 
	 * @param request
	 *            HttpServletRequest that contain session with prepared locale.
	 * @param errorMessage
	 *            String object that must be shown on the jsp, after executing.
	 * @param rbKey
	 *            String representation of key from the resource file with
	 *            messages.
	 */
	public static void setErrorMessage(HttpServletRequest request,
			String errorMessage, String rbKey) {

		String locale = "";
		locale = (String) request.getSession().getAttribute("lang");
		if ("ru_RU".equals(locale)) {
			request.setAttribute(errorMessage,
					ConfigurationRuMessageManager.getString(rbKey));
		} else {
			request.setAttribute(errorMessage,
					ConfigurationEnMessageManager.getString(rbKey));
		}
	}
}
