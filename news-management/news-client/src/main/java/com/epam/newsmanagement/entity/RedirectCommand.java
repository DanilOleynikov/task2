package com.epam.newsmanagement.entity;

import java.util.Arrays;
import java.util.List;

public class RedirectCommand {

	private static RedirectCommand instance;
	private static List<String> commands = Arrays.asList("addcomment");

	private RedirectCommand() {
	};

	public static RedirectCommand getInstance() {
		if (instance == null) {
			instance = new RedirectCommand();
		}
		return instance;
	}

	public boolean isNeedToRedirect(String command) {

		for (String s : commands) {
			if (s.toUpperCase().equals(command.toUpperCase())) {
				return true;
			}
		}
		return false;
	}
}
