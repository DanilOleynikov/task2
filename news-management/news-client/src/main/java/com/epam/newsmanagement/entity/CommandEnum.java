package com.epam.newsmanagement.entity;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.command.AddCommentCommand;
import com.epam.newsmanagement.command.ChangeLanguageCommand;
import com.epam.newsmanagement.command.NewsListPageCommand;
import com.epam.newsmanagement.command.SearchNewsCommand;
import com.epam.newsmanagement.command.ViewNewsCommand;

/**
 * This enumeration is contain all commands that Controller servlet can execute.
 * According to the incoming command it will built necessary block of
 * logic(certain class that executing some logic).
 *
 */
public enum CommandEnum {

	NEWSLISTPAGE {
		{
			this.command = new NewsListPageCommand();
		}
	},
	VIEWNEWS {
		{
			this.command = new ViewNewsCommand();
		}
	},
	ADDCOMMENT {
		{
			this.command = new AddCommentCommand();
		}
	},
	CHANGELANGUAGE {
		{
			this.command = new ChangeLanguageCommand();
		}
	},
	SEARCHNEWS {
		{
			this.command = new SearchNewsCommand();
		}
	};

	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}
