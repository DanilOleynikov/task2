package com.epam.newsmanagement.factory;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.entity.CommandEnum;

/**
 * This class is responsible for acquiring and defining commands from
 * HttpServletRequest object.
 *
 */
public class ActionFactory {

	/**
	 * This method is getting from HttpServletRequest object command parameter.
	 * The trying to find this command in CommandEnum, and if it will find it,
	 * it will return object of class implementation of that is responsible for
	 * this command.
	 * 
	 * @param request
	 *            HttpServletRequest object.
	 * @return object of class inherited from ActionCommand, with certain
	 *         implementation of execute() method.
	 */
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current;
		String action = request.getParameter("command");
		CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
		current = currentEnum.getCurrentCommand();

		return current;

	}
}
