package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.exception.ControllerException;

/**
 * This interface is using for ActionFactory pattern, for providing some
 * functionality in Controller servlet.
 *
 */
public interface ActionCommand {
	/**
	 * THis method is using for making common method for executing all necessary
	 * commands in application.
	 * 
	 * @param request
	 *            HttpServletRequest object, that must contain parameter
	 *            command.
	 * @param ac
	 *            Context of application.
	 * @return String value that contain page to forward.
	 * @throws ControllerException . thrown
	 *             if any error acquired in Controller.
	 */
	String execute(HttpServletRequest request, ApplicationContext ac)
			throws ControllerException;
}