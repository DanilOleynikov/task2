package com.epam.newsmanagement.validator;

/**
 * This class is providing functionality of validating information( in our case
 * length of income String).
 */
public class CommentFormValidator {

	/**
	 * 
	 * @param commentText
	 *            String value that need to be checked.
	 * @return true if length of income String will be in range of [10,100],
	 *         false otherwise.
	 */
	public static boolean isCommentValid(String commentText) {

		return (commentText.length() < 10 || commentText.length() > 100) ? false
				: true;
	}
}