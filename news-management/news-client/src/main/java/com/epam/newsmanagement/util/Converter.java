package com.epam.newsmanagement.util;

/**
 * This is support class for converting some arrays into another one's.
 */
public class Converter {

	/**
	 * This method is responsible for converting from String[] -> Long[]. Mostly
	 * it's using when user is searching news according to the tags and/or
	 * author.
	 * 
	 * @param stringArray
	 *            String[] array that contain necessary value to be converted.
	 * @return Long[] array of values that will be converted from String[]
	 *         array.
	 */
	public static Long[] convertStringToLongArray(String[] stringArray) {

		Long[] longArray = new Long[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			longArray[i] = Long.valueOf(stringArray[i]);
		}
		return longArray;
	}
}
