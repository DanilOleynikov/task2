package com.epam.newsmanagement.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This filter is using for correcting all requests in application. It
 * responsible for setting UTF-8 for all requests, that are made in application.
 * Mostly it using for supporting Russian Language in app.
 *
 */
public class CharsetFilter implements Filter {

	private String encoding = "utf-8";

	public void doFilter(ServletRequest request,

	ServletResponse response, FilterChain filterChain) throws IOException,
			ServletException {

		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);

		filterChain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		String encodingParam = filterConfig.getInitParameter("encoding");
		if (encodingParam != null) {
			encoding = encodingParam;
		}
	}

	public void destroy() {
	}

}
