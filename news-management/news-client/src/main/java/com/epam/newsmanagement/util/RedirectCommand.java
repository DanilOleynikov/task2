package com.epam.newsmanagement.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RedirectCommand {

	private static RedirectCommand instance = new RedirectCommand();
	private static Set<String> commands = new HashSet<String>(
			Arrays.asList("addcomment"));

	private RedirectCommand() {
	};

	public static RedirectCommand getInstance() {
		return instance;
	}

	public boolean isNeedToRedirect(String command) {
		if (commands.contains(command.toLowerCase())) {
			return true;
		}
		return false;
	}
}
