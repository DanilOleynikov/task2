package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;

import com.epam.newsmanagement.dao.util.PrevNextNewsHelper;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.exception.ControllerException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

public class ViewNewsCommand implements ActionCommand {

	private static final String VIEW_NEWS_PAGE = "/WEB-INF/pages/viewNewsPage.jsp";

	/**
	 * This method is using for providing functionality of retreaving full news
	 * from database. It will fill request object with NewsVO object and id's
	 * for next and previous news if they are existing.
	 */
	@Override
	public String execute(HttpServletRequest request, ApplicationContext ac)
			throws ControllerException {

		Long previousNewsId = null;
		Long nextNewsId = null;
		PrevNextNewsHelper newsHelper = new PrevNextNewsHelper();

		NewsService newsService = (NewsService) ac.getBean("newsService");

		Long newsId = Long.valueOf(request.getParameter("id"));
		List<Long> newsIdList = new ArrayList<Long>();

		NewsVO newsVo = new NewsVO();
		try {
			newsVo = newsService.loadNewsVo(newsId);
			newsIdList = newsService.loadAllNewsId();
			previousNewsId = newsHelper.getPrevNewsId(newsId, newsIdList);
			nextNewsId = newsHelper.getNextNewsId(newsId, newsIdList);
		} catch (ServiceException e) {
			throw new ControllerException(e.getMessage(), e);
		}

		request.setAttribute("newsVo", newsVo);
		if (previousNewsId != null) {
			request.setAttribute("previousNewsId", previousNewsId);
		}
		if (nextNewsId != null) {
			request.setAttribute("nextNewsId", nextNewsId);
		}

		return VIEW_NEWS_PAGE;
	}

}
