package com.epam.newsmanagement.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * This class is using for init. and handling all requests that are came from
 * editNewsPage.
 */
@RequestMapping("/EditNewsPage")
@Controller
public class EditNewsPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EditNewsPageController.class);
	private static final String EDIT_NEWS_PAGE = "editNewsPage";

	@Autowired
	private NewsService newsService;

	/**
	 * This method is using for initializing of editNewsPage and filling model
	 * attribute with value of news to edit(if it exists).
	 * 
	 * @param id
	 *            Long value of id of news that need to be loaded to edit.
	 * @param model
	 *            Model object that will be filled with NewsVO object, that need
	 *            to be updated.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/editNews/{id}" })
	public String editNewsPageInit(@PathVariable("id") Long id, Model model) {

		NewsVO newsVo = new NewsVO();

		try {
			newsVo = newsService.loadNewsVo(id);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of initialization of editNewsPage: ",
					e);
		}
		model.addAttribute("newsVoToEdit", newsVo);
		return EDIT_NEWS_PAGE;
	}

	/**
	 * This method is using for processing of editing news.In the end of method,
	 * it will redirect to viewNewsPage with News that was earlier edited.
	 * 
	 * @param news
	 *            News object that contain all necessary data that need to be
	 *            updated in database.
	 * @param model
	 *            Model object with data for view layer.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/updateNews" }, method = RequestMethod.POST)
	public String editConcreteNews(@ModelAttribute News news, Model model) {

		News tempNews = new News();
		try {
			tempNews = newsService.loadById(news.getId());
			news.setCreationDate(tempNews.getCreationDate());
			news.setModificationDate(new Date());
			newsService.update(news);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of opening editNewsPage with specified news id: ",
					e);
		}

		return "redirect:/EditNewsPage/editNews/" + news.getId();
	}
}
