package com.epam.newsmanagement.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Tag;

/**
 * This class providing functionality, mostly for validating String value of tag
 * objects that are coming from form of creating new news tags;
 */
@Component
public class TagValidator implements Validator {

	/**
	 * Minimum and maximum length of tag, that must be to be valid.
	 */
	private static final int MIN_LENGTH = 10;
	private static final int MAX_LENGTH = 30;

	@Override
	public boolean supports(Class<?> arg0) {
		return Tag.class.isAssignableFrom(arg0);
	}

	/**
	 * Main method that calling for validating of String representation of tag
	 * object. It will check for length of tag, and if it will be not in range
	 * of [10,30] it will set error message(value of error message is getting
	 * from resource file that contain necessary messages), that will be
	 * retreaved in jsp.
	 */
	@Override
	public void validate(Object object, Errors errors) {
		Tag tag = new Tag();
		tag = (Tag) object;
		String tagName = tag.getName();
		if (tagName.length() > MAX_LENGTH || tagName.length() < MIN_LENGTH) {
			errors.rejectValue("name", "label.validate.tag-no-valid");
		}

	}

}
