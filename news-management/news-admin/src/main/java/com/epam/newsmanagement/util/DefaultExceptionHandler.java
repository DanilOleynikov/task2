package com.epam.newsmanagement.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is some kind of advice to the all controllers in application. In
 * other words it says how Controllers should react if any of mentioned
 * exceptions arose during invocation.
 */
@ControllerAdvice
public class DefaultExceptionHandler {

	/**
	 * In value we setting type of Exception class, in order to handle all
	 * exceptions that need to be handled.
	 * 
	 * @param req
	 *            HttpServletRequest object.
	 * @param e
	 *            Exception object.
	 * @return ModelAndView with filled view name.
	 */
	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("errorPage");
		return modelAndView;
	}
}
