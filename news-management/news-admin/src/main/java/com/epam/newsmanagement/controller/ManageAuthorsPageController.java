package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.validator.AuthorValidator;

/**
 * This class is representing functionality for processing commands that came
 * from managingAuthorsPage.
 */
@RequestMapping("/ManageAuthorsPage")
@Controller
public class ManageAuthorsPageController {

	private static final String MANAGE_AUTHORS_PAGE = "manageAuthorsPage";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ManageAuthorsPageController.class);
	@Autowired
	private AuthorService authorService;
	@Autowired
	private AuthorValidator validator;

	/**
	 * This method is using for initializing manageAuthorsPage. Under
	 * initialization we understand of loading page with prefilling authors that
	 * can be edited and input to add news authors to database.
	 * 
	 * @param model
	 *            Model object that will be filled with list of existing in
	 *            database authors.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/managingOfAuthors" }, method = RequestMethod.GET)
	public String initManageAuthorsPage(Model model) {

		List<Author> authors = new ArrayList<Author>();

		try {
			authors = authorService.loadAll();
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method that responsible for init manageAuthorsPage: ",
					e);
		}

		model.addAttribute("authors", authors);
		model.addAttribute("author", new Author());
		return MANAGE_AUTHORS_PAGE;
	}

	/**
	 * This method is using for fulfilling such functionality of adding of new
	 * author to database;
	 * 
	 * @param model
	 *            if author not valid, it fill Model object will all authors,
	 *            that need to be displayed on the page.
	 * @param author
	 *            Author object that contain all necessary data for adding to
	 *            database.
	 * @param result
	 *            result of binding with author object form form.
	 * @return String value of page that need to be redirected/forwarded.
	 * 
	 */
	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.POST)
	public String addAuthor(Model model, Author author, BindingResult result) {

		validator.validate(author, result);
		if (result.hasErrors()) {
			List<Author> authors = new ArrayList<Author>();
			try {
				authors = authorService.loadAll();
			} catch (ServiceException e) {
				LOGGER.error("Can't load all authors in addAuthor method: ", e);
			}
			model.addAttribute("authors", authors);
			return MANAGE_AUTHORS_PAGE;
		} else {
			author.setExpiredTime(new Date());
			try {
				authorService.add(author);
			} catch (ServiceException e) {
				LOGGER.error(
						"Can't invoke method of adding new author in controller class: ",
						e);
			}
		}
		return "redirect:/ManageAuthorsPage/managingOfAuthors";
	}

	/**
	 * This mapping us using to call of updating of mentioned author name in
	 * database.
	 * 
	 * @param authorName
	 *            String value of author name that need to be updated.
	 * @param authorId
	 *            id of author that need to be updated.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/updateAuthorName" }, method = RequestMethod.POST)
	public String updateAuthorName(
			@RequestParam("updateAuthorName") String authorName,
			@RequestParam("id") Long authorId) {

		try {
			authorService.updateAuthorName(authorId, authorName);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of updating  author name in controller class: ",
					e);
		}
		return "redirect:/ManageAuthorsPage/managingOfAuthors";
	}

	/**
	 * This method is using for making author expired.
	 * 
	 * @param id
	 *            Long value of author id that need to be expired.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/updateAuthorExpired" }, method = RequestMethod.GET)
	public String updateAuthorExpired(@RequestParam Long id) {

		try {
			authorService.updateAuthorExpired(id);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of updating  author expired in controller class: ",
					e);
		}
		return "redirect:/ManageAuthorsPage/managingOfAuthors";
	}
}
