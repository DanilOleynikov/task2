package com.epam.newsmanagement.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.News;

/**
 * This class providing functionality, mostly for validating String value of
 * news fields object such as title, short text, full text. That are coming from
 * creating/updating news.
 */
@Component
public class NewsCreationValidator implements Validator {

	/**
	 * Minimum and maximum length news title, that must be to be valid.
	 */
	private static final int MIN_TITLE_LENGTH = 10;
	private static final int MAX_TITLE_LENGTH = 30;
	/**
	 * Minimum and maximum length news short text, that must be to be valid.
	 */
	private static final int MIN_SHORTTEXT_LENGTH = 10;
	private static final int MAX_SHORTTEXT_LENGTH = 100;
	/**
	 * Minimum and maximum length news full text, that must be to be valid.
	 */
	private static final int MIN_FULLTEXT_LENGTH = 10;
	private static final int MAX_FULLTEXT_LENGTH = 2000;

	@Override
	public boolean supports(Class<?> arg0) {
		return News.class.isAssignableFrom(arg0);
	}

	/**
	 * This method is validating 3 fields of news object: title,short text, full
	 * text for it length. If title length will in [10,30], short text
	 * in[10,100], full text in[10,2000], fields is valid, otherwise it will set
	 * certain error message about validation that will be retreaved in jsp.
	 */
	@Override
	public void validate(Object object, Errors errors) {
		News news = new News();
		news = (News) object;
		int newsTitleLength = news.getTitle().length();
		int newsShortTextLength = news.getShortText().length();
		int newsFullTextLength = news.getFullText().length();

		if (newsTitleLength > MAX_TITLE_LENGTH
				|| newsTitleLength < MIN_TITLE_LENGTH) {
			errors.rejectValue("title", "label.validate.news-title-not-valid");

		}

		if (newsShortTextLength > MAX_SHORTTEXT_LENGTH
				|| newsShortTextLength < MIN_SHORTTEXT_LENGTH) {
			errors.rejectValue("shortText",
					"label.validate.news-short-text-not-valid");

		}
		if (newsFullTextLength > MAX_FULLTEXT_LENGTH
				|| newsFullTextLength < MIN_FULLTEXT_LENGTH) {
			errors.rejectValue("fullText",
					"label.validate.news-full-text-not-valid");

		}

	}

}
