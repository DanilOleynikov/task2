package com.epam.newsmanagement.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Author;

/**
 * This class providing functionality, mostly for validating String value author
 * name field, that came from page of creating/updating new author.
 */
@Component
public class AuthorValidator implements Validator {

	/**
	 * Minimum and maximum length of author name, that must be to be valid.
	 */
	private static final int MIN_LENGTH = 10;
	private static final int MAX_LENGTH = 30;

	@Override
	public boolean supports(Class<?> arg0) {
		return Author.class.isAssignableFrom(arg0);
	}

	/**
	 * Main method that calling for validating of String representation of
	 * author name object. It will check for length of author name, and if it
	 * will be not in range of [10,30] it will set error message(value of error
	 * message is getting from resource file that contain necessary messages),
	 * that will be extracted in jsp.
	 */

	@Override
	public void validate(Object object, Errors errors) {
		Author author = new Author();
		author = (Author) object;
		String authorName = author.getName();
		if (authorName.length() < MIN_LENGTH
				|| authorName.length() > MAX_LENGTH) {
			errors.rejectValue("name", "label.validate.author-not-valid");

		}
	}
}
