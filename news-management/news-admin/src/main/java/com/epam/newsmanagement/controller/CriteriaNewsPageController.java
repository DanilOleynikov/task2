package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * This class is using for init. and handling all requests that are came from
 * criteriaNewsPage.
 */
@RequestMapping("/CriteriaNewsPage")
@Controller
public class CriteriaNewsPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CriteriaNewsPageController.class);
	private static final String CRITERIA_NEWS_PAGE = "criteriaNewsPage";

	@Autowired
	private NewsService newsService;

	/**
	 * This method is using for initializing and searchings news according to
	 * passed parameters(author and/or tag(s)).
	 * 
	 * @param authorId
	 *            id of author that need to be added to SearchCriteria. id of
	 * @param tagId
	 *            tag(s) that need to be added to SearchCriteria.
	 * @param model
	 *            Model object that will be filled with found news.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = "/newsSearching")
	public String initCriteriaNewsPage(
			@RequestParam(required = false) Long authorId, Long[] tagId,
			Model model) {

		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(authorId);
		sc.setTagId(tagId);
		List<NewsVO> criteriaNewsVoList = new ArrayList<NewsVO>();
		List<Long> newsIdList = new ArrayList<Long>();

		try {
			newsIdList = newsService.loadNewsIdSearchedByCriteria(sc, new Long(
					0));
			criteriaNewsVoList = newsService
					.loadListOfNewsVoByNewsId(newsIdList);

		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of initialization of criteriaNewsPage: ",
					e);
		}

		model.addAttribute("criteriaNewsVoList", criteriaNewsVoList);
		return CRITERIA_NEWS_PAGE;
	}
}
