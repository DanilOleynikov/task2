package com.epam.newsmanagement.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Comment;

/**
 * This class providing functionality, mostly for validating String value of
 * comment text that are coming from view news page when user is trying to add
 * new comment.
 */

@Component
public class CommentValidator implements Validator {

	/**
	 * Minimum and maximum length of comment text, that must be to be valid.
	 */
	private static final int MIN_COMMENT_LENGTH = 10;
	private static final int MAX_COMMENT_LENGTH = 100;

	@Override
	public boolean supports(Class<?> arg0) {
		return Comment.class.isAssignableFrom(arg0);
	}

	/**
	 * Main method that calling for validating of String representation of
	 * comment text. It will check for length of comment text, and if it will be
	 * not in range of [10,100] it will set error message(value of error message
	 * is getting from resource file that contain necessary messages), that will
	 * be retreaved in jsp.
	 */

	@Override
	public void validate(Object arg0, Errors errors) {
		Comment comment = (Comment) arg0;
		String commentText = comment.getText();
		if (commentText.length() > MAX_COMMENT_LENGTH
				|| commentText.length() < MIN_COMMENT_LENGTH) {
			errors.rejectValue("text", "label.validate.comment-not-valid");
		}

	}
}
