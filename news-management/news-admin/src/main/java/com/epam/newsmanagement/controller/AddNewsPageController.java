package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagmentService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.validator.NewsCreationValidator;

/**
 * This class is using for init. and handling all requests that are came from
 * addNewsPage.
 */
@RequestMapping("/AddNewsPage")
@Controller
public class AddNewsPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AddNewsPageController.class);
	private static final String ADD_NEWS_PAGE = "addNewsPage";

	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsManagmentService newsManagmentService;
	@Autowired
	private NewsCreationValidator validator;

	/**
	 * This method is using for adding of new news into database with mentioned
	 * tags and authors(if be more specific author/tag(s) id(s)).
	 * 
	 * @param news
	 *            News object.
	 * @param model
	 *            Model object that will be filled with data for page
	 *            initialization.
	 * @param result
	 *            BindingResult using for binding results in form, for
	 *            appropriate properties in object.
	 * @param authorId
	 *            Long value of author ID that need to be searched in database
	 *            and if it possible added to news.
	 * @param tagId
	 *            Long value of tag ID that need to be searched in database and
	 *            if it possible added to news.
	 * @return String value of page that need to be forwarded or redirected.
	 */
	@RequestMapping(value = { "/createNewNews" }, method = RequestMethod.POST)
	public String addNews(News news, Model model, BindingResult result,
			@RequestParam("authorId") Long authorId,
			@RequestParam("tagId") Long[] tagId) {

		validator.validate(news, result);
		if (result.hasErrors()) {

			List<Tag> tags = new ArrayList<Tag>();
			List<Author> nonExpiredAuthors = new ArrayList<Author>();

			try {
				tags = tagService.loadAll();
				nonExpiredAuthors = authorService.loadAllNonExpired();
			} catch (ServiceException e) {
				LOGGER.error(
						"Can't invoke operation of initialization of AddNewsPage: ",
						e);
			}

			model.addAttribute("tags", tags);
			model.addAttribute("authors", nonExpiredAuthors);
			return ADD_NEWS_PAGE;

		} else {
			Long newsId = new Long(0);
			news.setCreationDate(new Date(new Date().getTime()));
			news.setModificationDate(new Date(new Date().getTime()));
			try {
				newsId = newsManagmentService.saveNewsWithAuthorAndTags(news,
						authorId, tagId);
				news.setId(newsId);

			} catch (ServiceException e) {
				LOGGER.error("Can't invoke operation of adding of new news: ",
						e);
			}
		}

		return "redirect:/ViewNewsPage/viewNews/" + news.getId();
	}

	/**
	 * This method is using for initializing addNewsPage. Under initializing we
	 * understand, that need to load page with prefilling lists of available
	 * tags and authors.
	 * 
	 * @param model
	 *            Model object that will be filled with list of available tags
	 *            and authors.
	 * @return String value of page that need to be redirected/forwarded.
	 */

	@RequestMapping(value = { "/addNews" }, method = RequestMethod.GET)
	public String initAddNewsPage(Model model) {

		List<Tag> tags = new ArrayList<Tag>();
		List<Author> nonExpiredAuthors = new ArrayList<Author>();

		try {
			tags = tagService.loadAll();
			nonExpiredAuthors = authorService.loadAllNonExpired();
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke operation of initialization of AddNewsPage: ",
					e);
		}

		model.addAttribute("tags", tags);
		model.addAttribute("authors", nonExpiredAuthors);
		model.addAttribute("news", new News());
		return ADD_NEWS_PAGE;
	}
}
