package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.dao.util.PrevNextNewsHelper;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.validator.CommentValidator;

/**
 * This class is representing functionality for processing commands that came
 * from viewNewsPage.
 */
@RequestMapping("/ViewNewsPage")
@Controller
public class ViewNewsPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ViewNewsPageController.class);

	private static final String VIEW_NEWS_PAGE = "viewNewsPage";
	@Autowired
	private CommentValidator validator;
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;

	/**
	 * This method is using for initializing viewNewsPage with prefilling NewsVO
	 * object.
	 * 
	 * @param id
	 *            Long value of newsId that need to be loaded.
	 * @param model
	 *            Model object that will be prefilling with all necessary data.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/viewNews/{id}" }, method = { RequestMethod.GET })
	public String viewNews(@PathVariable("id") Long id, Model model) {

		Long previousNewsId = null;
		Long nextNewsId = null;
		PrevNextNewsHelper newsHelper = new PrevNextNewsHelper();
		NewsVO newsVo = new NewsVO();
		List<Long> newsIdList = new ArrayList<Long>();

		try {
			newsIdList = newsService.loadAllNewsId();
			newsVo = newsService.loadNewsVo(id);
			previousNewsId = newsHelper.getPrevNewsId(id, newsIdList);
			nextNewsId = newsHelper.getNextNewsId(id, newsIdList);

		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of init viewNewsPage with specified news id: ",
					e);
		}

		if (previousNewsId != null) {
			model.addAttribute("previousNewsId", previousNewsId);
		}
		if (nextNewsId != null) {
			model.addAttribute("nextNewsId", nextNewsId);
		}

		model.addAttribute("newsVo", newsVo);
		model.addAttribute("comment", new Comment());
		return VIEW_NEWS_PAGE;
	}

	/**
	 * This method is using for fulfilling of adding comment to news.
	 * 
	 * @param newsId
	 *            Long value of newsId, need to be redirected back to page.
	 * @param model
	 *            Model object that will be filled with necessary data.
	 * @param comment
	 *            Comment object that contain all necessary data to add.
	 * @param result
	 *            result of binding parameters from form with Comment object.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/addComment" }, method = RequestMethod.POST)
	public String addComment(@RequestParam Long newsId, Model model,
			Comment comment, BindingResult result) {

		validator.validate(comment, result);
		if (result.hasErrors()) {
			NewsVO newsVo = new NewsVO();
			try {
				newsVo = newsService.loadNewsVo(newsId);
			} catch (ServiceException e) {
				LOGGER.error(
						"Can't load newsVo from viewNewsPage cotrnoller class: ",
						e);
			}
			model.addAttribute("newsVo", newsVo);
			return VIEW_NEWS_PAGE;

		} else {
			try {
				comment.setCreationTime(new Date());
				comment.setNewsId(newsId);
				commentService.add(comment);

			} catch (ServiceException e) {
				LOGGER.error(
						"Can't invoke method of adding of new comment in controller class: ",
						e);
			}
		}
		return "redirect:/ViewNewsPage/viewNews/" + newsId;

	}

	/**
	 * This method is using for fulfilling of deleting comment from news.
	 * 
	 * @param commentId
	 *            Long value of commentId that need to be deleted from news.
	 * @param newsId
	 *            Long value of newsId, need to be redirected back to page.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/deleteComment" }, method = RequestMethod.POST)
	public String deleteComment(@RequestParam Long commentId, Long newsId) {
		try {
			commentService.delete(commentId);

		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of deleting comment from news in controller class: ",
					e);
		}
		return "redirect:/ViewNewsPage/viewNews/" + newsId;
	}
}
