package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class has only 1 method for initializing 404 error page.
 *
 */
@Controller
public class Error404PageController {

	private static final String PAGE_NOT_FOUND = "404Page";

	/**
	 * Due to set mapping it will return certain page.
	 * 
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = "/404")
	public String init404Page() {

		return PAGE_NOT_FOUND;

	}
}
