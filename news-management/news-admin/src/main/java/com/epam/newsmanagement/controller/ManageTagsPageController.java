package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.validator.TagValidator;

/**
 * This class is representing functionality for processing commands that came
 * from manageTagsPage.
 */
@RequestMapping("/ManageTagsPage")
@Controller
public class ManageTagsPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ManageTagsPageController.class);

	private static final String MANAGE_TAGS_PAGE = "manageTagsPage";
	@Autowired
	private TagService tagService;
	@Autowired
	private TagValidator validator;

	/**
	 * This method is using for adding new tag to the database.
	 * 
	 * @param tag
	 *            Tag object that contain all necessary data to be updated.
	 * @param model
	 *            Model object that will be filled if error will acquired during
	 *            validation on server.
	 * @param result
	 *            result of binding with Tag object from jsp form.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "addTag" }, method = RequestMethod.POST)
	public String addTag(Tag tag, Model model, BindingResult result) {
		validator.validate(tag, result);
		if (result.hasErrors()) {
			List<Tag> tags = new ArrayList<Tag>();
			try {
				tags = tagService.loadAll();
			} catch (ServiceException e) {
				LOGGER.error("Can't load tags from method addTag: ", e);
			}
			model.addAttribute("tags", tags);
			return "manageTagsPage";

		} else {
			try {
				tagService.add(tag);
			} catch (ServiceException e) {
				LOGGER.error(
						"Can't invoke method of adding new tag in controller class: ",
						e);
			}
		}

		return "redirect:/ManageTagsPage/managingOfTags";
	}

	/**
	 * This mapping is using for calling of deleting of tag from database.
	 * 
	 * @param id
	 *            Long value of id of tag that need to be deleted from database.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = "/delete")
	public String deleteTag(@RequestParam String id) {
		Long tagId = new Long(id);
		try {
			tagService.delete(tagId);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of  deleting tag in controller class: ",
					e);
		}
		return "redirect:/ManageTagsPage/managingOfTags";

	}

	/**
	 * This method is using for updating tag name in database.
	 * 
	 * @param tag
	 *            Tag object that contain all necessary data for updating.
	 * @param model
	 *            Model object that will filled if error acquired during
	 *            validation of tag name on server.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = "/update")
	public String updateTag(@ModelAttribute("tag") Tag tag, Model model) {

		try {
			tagService.update(tag);
		} catch (ServiceException e) {
			LOGGER.error(
					"Can't invoke method of  updating tag in controller class: ",
					e);
		}
		return "redirect:/ManageTagsPage/managingOfTags";

	}

	/**
	 * This method is using for initializing manageTagsPage. Under
	 * initialization we understand of loading page with prefilling tags that
	 * can be edited and input to add new tags.
	 * 
	 * @param model
	 *            Model object that will be filled with list of existing in
	 *            database tags.
	 * @return String value of page that need to be redirected/forwarded.
	 */

	@RequestMapping(value = { "/managingOfTags" }, method = RequestMethod.GET)
	public String initManageTagsPage(Model model) {

		List<Tag> tags = new ArrayList<Tag>();
		try {
			tags = tagService.loadAll();
		} catch (ServiceException e) {
			LOGGER.error("Can't init manageTagsPage: ", e);
		}
		model.addAttribute("tags", tags);
		model.addAttribute("tag", new Tag());
		return MANAGE_TAGS_PAGE;
	}
}
