package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class has only 1 method for initializing login page.
 *
 */
@Controller
public class LoginPageController {

	private static final String LOGIN_PAGE = "loginPage";

	/**
	 * Due to set mapping it will return certain page.
	 * 
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/login", "/" }, method = RequestMethod.GET)
	public String loginPage() {
		return LOGIN_PAGE;
	}
}
