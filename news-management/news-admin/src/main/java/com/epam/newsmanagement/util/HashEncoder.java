package com.epam.newsmanagement.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class is carries informational role. If in future need to add some new
 * users to db, you can use this class for creating md5 hashed passwords.
 *
 */
public class HashEncoder {

	/**
	 * This method is using algorithm of md5 hashing. It will return 32-chars
	 * representation of input String.
	 * 
	 * @param password
	 *            String representation of password.
	 * @return hashed String value of passed password.
	 * @throws NoSuchAlgorithmException
	 *             arose if no methods is found according to the mentioned one.
	 */
	public static String getPasswordHash(String password)
			throws NoSuchAlgorithmException {

		MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		messageDigest.update(password.getBytes(), 0, password.length());
		String hashedPass = new BigInteger(1, messageDigest.digest())
				.toString(16);
		if (hashedPass.length() < 32) {
			hashedPass = "0" + hashedPass;
		}

		return hashedPass;
	}
}
