package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagmentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.PaginationHelper;

/**
 * This class is representing functionality for processing commands that came
 * from newsListPage.
 */

@RequestMapping("/NewsListPage")
@Controller
public class NewsListPageController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NewsListPageController.class);

	private static final String NEWS_LIST_PAGE = "newsListPage";
	@Autowired
	private NewsService newsService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsManagmentService newsManagmentService;

	/**
	 * THis method is using for loading certain news list according to the
	 * mentioned page.
	 * 
	 * @param page
	 *            Long value of page that need to be loaded.
	 * @param model
	 *            Model object that will be filled with necessary data.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/page/{page}" })
	public String selectSpecificPage(@PathVariable("page") Long page,
			Model model) {
		List<Author> authorList = new ArrayList<Author>();
		List<Tag> tagList = new ArrayList<Tag>();
		List<NewsVO> newsVoList = new ArrayList<NewsVO>();
		List<Long> newsIdList = new ArrayList<Long>();

		Long newsCount = new Long(0);
		Long numberOfPages = new Long(0);
		Long currentPage = page;

		try {
			newsCount = newsService.getNewsCount();
			authorList = authorService.loadAll();
			tagList = tagService.loadAll();
			newsIdList = newsService
					.loadNewsIdByTheSpecificPageSortedByNumberOfCommentsAndModificationDate(page - 1);
			newsVoList = newsService.loadListOfNewsVoByNewsId(newsIdList);

		} catch (ServiceException e) {
			LOGGER.error("Can't invoke method of init newsListPage: ", e);
		}
		if (newsCount != 0) {
			numberOfPages = PaginationHelper.getNumberOfPages(newsCount);
		}

		model.addAttribute("numberOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("newsVoList", newsVoList);
		model.addAttribute("authorList", authorList);
		model.addAttribute("tagList", tagList);

		return NEWS_LIST_PAGE;

	}

	/**
	 * This method is using for deleting news from database according to the
	 * passed id's of news.
	 * 
	 * @param newsIdToDelete
	 *            array of news id's that need to be deleted.
	 * @param model
	 *            Model object that will be filled with necessary data.
	 * @return String value of page that need to be redirected/forwarded.
	 */
	@RequestMapping(value = { "/deleteNews" }, method = RequestMethod.POST)
	public String deleteNews(
			@RequestParam(value = "newsIdToDelete", required = false) Long[] newsIdToDelete,
			Model model) {

		if (newsIdToDelete != null) {
			for (Long id : newsIdToDelete) {
				try {
					newsManagmentService.deleteNews(id);
				} catch (ServiceException e) {
					LOGGER.error(
							"Can't invoke method of deleting news in controller class: ",
							e);
				}
			}
		}

		return "redirect:/NewsListPage/page/1";
	}
}
