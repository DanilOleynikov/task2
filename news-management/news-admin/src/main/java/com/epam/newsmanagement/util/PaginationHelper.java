package com.epam.newsmanagement.util;

/**
 * This class providing a little help for calculating number of pages due to
 * number of news. It's using ceil(Long newsCount) method because we need not
 * double number of pages. For example we have 7 news and the counter of news on
 * page = 3; Than we will divide 7 on 3, and will get 3(instead of 2.333).
 *
 */
public class PaginationHelper {

	private static final long NEWS_PER_PAGE = 3;

	public static Long getNumberOfPages(Long newsCount) {
		return (long) (Math.ceil(newsCount * 1.0 / NEWS_PER_PAGE));
	}
}
