function isTitleValid(textbox) {
	if (textbox.value == '') {
		textbox.setCustomValidity(validationMessages["EmptyTitle"]);
	} else if (textbox.value.length < 10) {
		textbox.setCustomValidity(validationMessages["TitleLengthLess10Chars"]);
	} else if (textbox.value.length > 30) {
		textbox.setCustomValidity(validationMessages["TitleLengthGreaterThan30Chars"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
}

function isShortTextValid(textbox) {

	if (textbox.value == '') {
		textbox.setCustomValidity(validationMessages["ShortTextEmpty"]);
	} else if (textbox.value.length < 10) {
		textbox.setCustomValidity(validationMessages["ShortTextLess10Chars"]);
	} else if (textbox.value.length > 100) {
		textbox.setCustomValidity(validationMessages["ShortTextBiggerThan100Chars"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
}

function isFullTextValid(textbox) {

	if (textbox.value == '') {
		textbox.setCustomValidity(validationMessages["FullTextEmpty"]);
	} else if (textbox.value.length < 10) {
		textbox.setCustomValidity(validationMessages["FullTextLessThan10Chars"]);
	} else if (textbox.value.length > 2000) {
		textbox.setCustomValidity(validationMessages["FullTextGreaterThan2000Chars"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
}

function deleteNewsValidate(chk) {

	for (i = 0; i < chk.length; i++)
		if (chk[i].checked == true) {
			document.getElementById("deleteNews").style.visibility = "visible";
			break;
		} else {
			document.getElementById("deleteNews").style.visibility = "hidden";
		}
}

function validateAuthorAndTagOnCreation() {
	if (0 == document.creationForm.authorId.selectedIndex
			|| isNaN(document.creationForm.tagId.selectedIndex)) {
		return false;
	}
	return true;
}

function isAuthorNameValid(textbox) {

	if (textbox.value == '') {
		textbox
				.setCustomValidity(authorNameValidationMessages["EmptyAuthorName"]);
	} else if (textbox.value.length < 10) {
		textbox
				.setCustomValidity(authorNameValidationMessages["AuthorNameLess10Chars"]);
	} else if (textbox.value.length > 30) {
		textbox
				.setCustomValidity(authorNameValidationMessages["AuthorNameBigger30Chars"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
};

function isTagNameValid(textbox) {

	if (textbox.value == '') {
		textbox
				.setCustomValidity(tagNameValidationMessages["TagFieldEmpty"]);
	} else if (textbox.value.length < 10) {
		textbox
				.setCustomValidity(tagNameValidationMessages["TagNameLess10Chars"]);
	} else if (textbox.value.length > 30) {
		textbox
				.setCustomValidity(tagNameValidationMessages["TagNameGreaterThan30Chars"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
}

function isCommentInvalid(textbox) {

	if (textbox.value == '') {
		textbox

				.setCustomValidity(commentValidationMessages["CommentContentEmpty"]);
	} else if (textbox.value.length < 10) {
		textbox

				.setCustomValidity(commentValidationMessages["LenghtOfCommentLess10Chars"]);
	} else if (textbox.value.length > 100) {
		textbox

				.setCustomValidity(commentValidationMessages["CommentLengthBiggerThan100"]);
	} else {
		textbox.setCustomValidity('');
	}
	return true;
}