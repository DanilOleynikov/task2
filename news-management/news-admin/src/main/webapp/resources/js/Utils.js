function unhide(divID) {
	var item = document.getElementById(divID);
	if (item) {
		item.className = (item.className == 'hidden') ? 'unhidden' : 'hidden';
	}
}

$(function() {
	$("#lstTags").dropdownchecklist({
		icon : {},
		emptyText : tagDropDownPlaceHolder["SelectTagsInvitation"],
		width : 200

	});
	$("#lstAuthors").dropdownchecklist({
		icon : {},
		width : 200
	});
});