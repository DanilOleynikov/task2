	function isTitleValid(textbox) {

		var emptyTitleInfo = document.getElementById("titleEmptyMessage").value;
		var titleLessThan10 = document.getElementById("titleLessThan10").value;;
		var titleMoreThan100 = document.getElementById("titleMoreThan30").value;;

		if (textbox.value == '') {
			textbox
					.setCustomValidity(emptyTitleInfo);
		} else if (textbox.value.length < 10) {
			textbox
					.setCustomValidity(titleLessThan10);
		} else if (textbox.value.length > 30) {
			textbox
					.setCustomValidity(titleMoreThan100);
		} else {
			textbox.setCustomValidity('');
		}
		return true;
	}
	
	function isShortTextValid(textbox) {

		if (textbox.value == '') {
			textbox
					.setCustomValidity('<spring:message code="label.field-empty-warning"/>');
		} else if (textbox.value.length < 10) {
			textbox
					.setCustomValidity('<spring:message code="label.less-than-10-chars"/>');
		} else if (textbox.value.length > 100) {
			textbox
					.setCustomValidity('<spring:message code="label.bigger-than-100-characters-warning"/>');
		} else {
			textbox.setCustomValidity('');
		}
		return true;
	}

	function isFullTextValid(textbox) {

		if (textbox.value == '') {
			textbox
					.setCustomValidity('<spring:message code="label.field-empty-warning"/>');
		} else if (textbox.value.length < 10) {
			textbox
					.setCustomValidity('<spring:message code="label.less-than-10-chars"/>');
		} else if (textbox.value.length > 2000) {
			textbox
					.setCustomValidity('<spring:message code="label.bigger-than-2000-characters-warning"/>');
		} else {
			textbox.setCustomValidity('');
		}
		return true;
	}