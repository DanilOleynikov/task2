<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><spring:message code="label.title.manage.authors" /></title>
</head>

<body>

	<div id="manage-authors-content">
		<table width="100%">
			<c:forEach var="author" items="${authors}">
				<tr>
					<form:form action="${pageContext.request.contextPath}/ManageAuthorsPage/updateAuthorName" name="formupdate${author.id}">

						<td><spring:message code="label.author" />:</td>
						<td><input value="${author.name}" disabled
							name="updateAuthorName" id="input${author.id}"></input></td>
						<input type="hidden" name="id" value="${author.id}" />
					</form:form>
				</tr>


				<tr>
					<td>
						<div id="editpopup${author.id}">
							<a href="#/"
								onclick="unhide('editpopup${author.id}');unhide('menupopup${author.id}');document.getElementById('input${author.id}').disabled = false;"><spring:message
									code="label.link.author-edit" /></a>

						</div>
					</td>

					<td>
						<div id='menupopup${author.id}' class="hidden">

							<a href="#/" onclick="document.formupdate${author.id}.submit();"><spring:message
									code="label.link.author-update" /></a> <a
								href="<c:url value="/ManageAuthorsPage/updateAuthorExpired?id=${author.id}"></c:url>"><spring:message
									code="label.link.author-expire" /></a> <a href="#/"
								onclick="unhide('editpopup${author.id}');unhide('expire${author.id}');unhide('menupopup${author.id}');document.getElementById('input${author.id}').disabled = true;"><spring:message
									code="label.link.author-cancel" /></a>
						</div>
					</td>
				</tr>
			</c:forEach>
			<tr style="height: 20px;"></tr>

			<form:form action="${pageContext.request.contextPath}/ManageAuthorsPage/addAuthor" method="POST" commandName="author">
				<tr>
					<td><spring:message code="label.add-author" />:</td>

					<td><form:input path="name" id="authorName"
							required="required" oninput="isAuthorNameValid(this)"
							oninvalid="isAuthorNameValid(this)" /></td>
					<td><input type="submit"
						value="<spring:message code="label.button.save-author" />" /></td>
				<tr>
					<td></td>
					<td><form:errors path="name" cssStyle="color: red" /></td>
				</tr>
			</form:form>
			</tr>
		</table>
	</div>
</body>
</html>