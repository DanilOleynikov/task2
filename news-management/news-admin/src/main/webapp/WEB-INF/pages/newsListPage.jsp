<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/templateStyle.css" />"
	rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><spring:message code="label.news-list-page.title" /></title>
</head>
<body onload="deleteNewsValidate(document.mainForm.newsIdToDelete)">
	<div class="search">

		<form action='<c:url value="/CriteriaNewsPage/newsSearching"></c:url>' method="POST">

			<select name=authorId class="authorselect" id="lstAuthors">
				<option selected="selected" disabled><spring:message
						code="label.select-author" /></option>
				<c:forEach var="author" items="${authorList}">
					<option value="${author.id}">${author.name}</option>
				</c:forEach>

			</select> <select name="tagId" class="tagselect" id="lstTags"
				multiple="multiple">
				<c:forEach var="tag" items="${tagList}">
					<option value="${tag.id}">${tag.name}</option>
				</c:forEach>

			</select> <input type="submit"
				value="<spring:message code="label.button.filter" />"
				class="findbutton" /> <input type="submit"
				value="<spring:message code="label.button.reset" />"
				class="resetbutton" />
		</form>

	</div>

	<c:if test="${empty newsVoList}">
		<spring:message code="label.info.no-news-to-display" />
	</c:if>

	<form action="${pageContext.request.contextPath}/NewsListPage/deleteNews"
		method="POST" name="mainForm">
		<c:if test="${not empty newsVoList}">
			<c:forEach var="newsVo" items="${newsVoList}">

				<div class="news">
					<div id="news-title">
						<a href='<c:url value="/ViewNewsPage/viewNews/${newsVo.news.id}"></c:url>'><span
							class="newstitledec">${newsVo.news.title}</span></a>
					</div>
					<div id="news-author">by(${newsVo.author.name})</div>
					<div id="news-date">${newsVo.news.modificationDate}</div>


					<div id="news-shorttext">${newsVo.news.shortText}</div>

					<c:set var="i" value="1"></c:set>
					<c:set var="tagsListSize" value="${fn:length(newsVo.tags)}" />

					<div id="info-news">
						<ul>

							<li class="li" id="tags"><span class="tagsdec"> <c:forEach
										var="tag" items="${newsVo.tags}">

										<c:choose>
											<c:when test="${i != tagsListSize}">
											${tag.name},
											<c:set var="i" value="${i+1}" />
											</c:when>
											<c:otherwise>
											${tag.name}
										</c:otherwise>
										</c:choose>
									</c:forEach>
							</span></li>

							<c:set var="realCommentaryCounter" value="0"></c:set>
							<c:forEach var="comment" items="${newsVo.comments}">
								<c:if test="${comment.id ne 0}">
									<c:set var="realCommentaryCounter"
										value="${realCommentaryCounter+1}"></c:set>
								</c:if>
							</c:forEach>
							<li class="li" id="comments"><span class="commentsdec"><spring:message
										code="label.comments" />(${realCommentaryCounter})</span></li>

							<li class="li"><a
								href='<c:url value="/EditNewsPage/editNews/${newsVo.news.id}"></c:url>'><spring:message
										code="label.edit" /></a></li>
							<li class="li"><input type="checkbox"
								onchange="deleteNewsValidate(document.mainForm.newsIdToDelete)" name="newsIdToDelete"
								value="${newsVo.news.id}"></li>
								
						</ul>
					</div>
				</div>
			</c:forEach>
		</c:if>

		<div id="paging">
			<table border="1" cellpadding="5" cellspacing="5">
				<tr>
					<c:if test="${currentPage != 1}">
						<td><a
							href="<c:url value="/newsListPage/${currentPage-1}"></c:url>"><spring:message
									code="label.previous" /></a></td>
					</c:if>

					<c:forEach begin="1" end="${numberOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td>${i}</td>
							</c:when>
							<c:otherwise>
								<td><a href="<c:url value='/newsListPage/${i}' />">${i}</a></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>

					<c:if test="${currentPage lt numberOfPages}">
						<td><a
							href="<c:url value='/newsListPage/${currentPage + 1}' />"><spring:message
									code="label.next" /></a></td>

					</c:if>
				</tr>
			</table>
		</div>

		<div id="delete">
			<input type="submit" id="deleteNews" style="visibility:hidden;"
			value="<spring:message code="label.button.delete"/>"
			class="deletebutton" />
	</form>
	</div>

</body>
</html>
