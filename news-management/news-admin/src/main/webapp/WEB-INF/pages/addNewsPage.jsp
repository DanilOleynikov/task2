
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<title><spring:message code="label.add-news-page.title" /></title>

<div id="add-news-content">

	<spring:message code="label.textarea.news-title.placeholder"
		var="newsTitlePlaceholder" />
	<spring:message code="label.textarea.news-shorttext.placeholder"
		var="newsShortTextPlaceholder" />
	<spring:message code="label.textarea.news-fulltext.placeholder"
		var="newsFullTextPlaceholder" />

	<form:form method="POST" name="creationForm" action="${pageContext.request.contextPath}/AddNewsPage/createNewNews" commandName="news" onsubmit="return validateAuthorAndTagOnCreation();">
		<table>										
			<tr>
				<td><spring:message code="label.news.title" />:</td>
				<td><form:input path="title" required="required"
						oninput="isTitleValid(this)" oninvalid="isTitleValid(this)"
						style="width: 220px;" placeholder="${newsTitlePlaceholder}" /></td>
				<td><form:errors path="title" style="color:red;"></form:errors></td>

			</tr>

			<tr>
				<td style="vertical-align: top"><spring:message
						code="label.news.shorttext" />:</td>
				<td><form:textarea path="shortText" required="required"
						oninput="isShortTextValid(this)"
						oninvalid="isShortTextValid(this)"
						style="width: 220px; height: 65px"
						placeholder="${newsShortTextPlaceholder}"></form:textarea></td>
				<td><form:errors path="shortText" style="color:red;"></form:errors></td>
			<tr>
			<tr>

				<td style="vertical-align: top"><spring:message
						code="label.news.fulltext" />:</td>
				<td><form:textarea path="fullText"
						style="width: 220px; height: 200px" required="required"
						oninput="isFullTextValid(this)" oninvalid="isFullTextValid(this)"
						placeholder="${newsFullTextPlaceholder}"></form:textarea></td>
				<td><form:errors path="fullText" style="color:red;"></form:errors></td>
			<tr>
						<tr>
				<td></td>

				<td><select name='authorId' id="lstAuthors" required>
						<option value=""><spring:message
								code="label.select-author" /></option>
						<c:forEach items="${authors}" var="author">
							<option value="${author.id}">${author.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td></td>
				<td><select name='tagId' id="lstTags" multiple="multiple" required>
						<c:forEach items="${tags}" var="tag">
							<option value="${tag.id}">${tag.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" onclick="validate()"
					value="<spring:message code="label.button.save-news" />" /></td>
			</tr>
		</table>
	</form:form>
</div>