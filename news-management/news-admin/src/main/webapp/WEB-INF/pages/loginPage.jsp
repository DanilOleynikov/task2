<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="label.login-page.title"/></title>

</head>
<body>

	<div id="login-page-content">
		<form name='f' action="j_spring_security_check" method='POST'>
			<table>
				<tr>
					<td><spring:message code="label.user" />:</td>
					<td><input type='text' name='username' placeholder="<spring:message code="label.login.placeholder" />"></td>
				</tr>
				
				<tr>
					<td><spring:message code="label.password" /></td>
					<td><input type='password' name='password' placeholder="<spring:message code="label.password.placeholder" />" /></td>
				</tr>
				<tr>
					<td><input name="submit" type="submit" value="<spring:message code="label.button.enter" />" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>