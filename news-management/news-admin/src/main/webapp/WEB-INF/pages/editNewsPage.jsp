<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="add-news-content">

	${EditSuccess}
	<form:form action="${pageContext.request.contextPath}/EditNewsPage/updateNews"
		method="POST">
		<table>
			<input type="hidden" value="${newsVoToEdit.news.id}" name="id" />
			<tr>
				<td><spring:message code="label.edit-news.title" />:</td>
				<td><input type="text" name="title" required oninput="isTitleValid(this)" oninvalid="isTitleValid(this)"
					style="width: 220px;" value="${newsVoToEdit.news.title}" /></td>

			</tr>

			<tr>
				<td><spring:message code="label.edit-news.date" />:</td>
				<td><input type="text"
					value="${newsVoToEdit.news.modificationDate}"
					name="modificationDate" style="width: 220px;" disabled /></td>
			</tr>
			<tr>
				<td style="vertical-align: top"><spring:message
						code="label.edit-news.shorttext" />:</td>
				<td><textarea name="shortText"
						required oninput="isShortTextValid(this)" oninvalid="isShortTextValid(this)"
						style="width: 220px; height: 65px" maxlength="100">${newsVoToEdit.news.shortText}</textarea></td>
			<tr>
			<tr>

				<td style="vertical-align: top"><spring:message
						code="label.edit-news.fulltext" />:</td>
				<td><textarea name="fullText"
						required oninput="isFullTextValid(this)" oninvalid="isFullTextValid(this)"
						style="width: 220px; height: 200px" maxlength="1000">${newsVoToEdit.news.fullText}</textarea></td>
			<tr>
			<tr>
				<td></td>
				<td><select name='tagId' id="lstTags" multiple="multiple"
					disabled>
						<c:forEach items="${newsVoToEdit.tags}" var="tag">
							<option value="${tag.id}" selected disabled>${tag.name}</option>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td></td>
				<td><select name='authorId' id="lstAuthors" disabled>
						<option value="${newsVoToEdit.author.id}" selected disabled>${newsVoToEdit.author.name}</option>
				</select></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit"
					value="<spring:message code="label.edit-news.button.edit" />" /></td>
			</tr>
		</table>
	</form:form>
</div>
