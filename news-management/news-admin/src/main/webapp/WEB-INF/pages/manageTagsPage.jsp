
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<title><spring:message code="label.title.manage.tags" /></title>
</head>
<body>

	<div id="tag-manage-content">


		<c:if test="${not empty tags}">

			<c:forEach var="tag" items="${tags}">

				<div id='${tag.id}${tag.name}' class="hidden">
					<a href="delete?id=${tag.id}"><spring:message
							code="label.link.tag-delete" /></a> <a href="#/"
						onclick="document.formupdate${tag.id}.submit();"><spring:message
							code="label.link.tag-update" /></a> <a href="#/"
						onclick="unhide('${tag.id}${tag.name}');unhide('${tag.name}${tag.id}');document.getElementById(${tag.id}).disabled = true;"><spring:message
							code="label.link.tag-cancel" /></a>
				</div>

				<form:form action="${pageContext.request.contextPath}/ManageTagsPage/update" name="formupdate${tag.id}" commandName="tag">
					<form:input value="${tag.name}" id="${tag.id}" disabled="true"
						path="name"/>
					<input type="hidden" value="${tag.id}" id="id" name="id" />
				</form:form>


				<div id='${tag.name}${tag.id}'>
					<a href="#/"
						onclick="unhide('${tag.id}${tag.name}');unhide('${tag.name}${tag.id}');document.getElementById(${tag.id}).disabled = false;"><spring:message
							code="label.link.tag-edit" /></a>
				</div>

			</c:forEach>

		</c:if>

		<br>
		<c:if test="${ not empty successAdd}">
		${successAdd}
	</c:if>
		<form:form action="${pageContext.request.contextPath}/ManageTagsPage/addTag" method="POST" commandName="tag">
			<table>
				<tr>
					<td><form:input path="name" required="required" oninvalid="isTagNameValid(this)" oninput="isTagNameValid(this)"/></td>
					<form:errors path="name" cssStyle="color:red;"></form:errors>
					<td><input type="submit"
						value="<spring:message code="label.button.add-tag" />" /></td>
				</tr>

			</table>
		</form:form>
	</div>