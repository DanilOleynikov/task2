<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="label.criteria-news-page.title" /></title>
</head>
<body>

	<c:if test="${empty criteriaNewsVoList}">
		<spring:message code="label.info.no-news-to-display" />
	</c:if>

	<form action="${pageContext.request.contextPath}/NewsListPage/deleteNews"
		method="POST">
		<c:if test="${not empty criteriaNewsVoList}">
			<c:forEach var="newsVo" items="${criteriaNewsVoList}">

				<div class="news">
					<div id="news-title">
						<a href='<c:url value="/ViewNewsPage/viewNews/${newsVo.news.id}"></c:url>'><span
							class="newstitledec">${newsVo.news.title}</span></a>
					</div>
					<div id="news-author">by(${newsVo.author.name})</div>
					<div id="news-date">${newsVo.news.modificationDate}</div>


					<div id="news-shorttext">${newsVo.news.shortText}</div>

					<c:set var="i" value="1"></c:set>
					<c:set var="tagsListSize" value="${fn:length(newsVo.tags)}" />

					<div id="info-news">
						<ul>

							<li class="li" id="tags"><span class="tagsdec"> <c:forEach
										var="tag" items="${newsVo.tags}">

										<c:choose>
											<c:when test="${i != tagsListSize}">
											${tag.name},
											<c:set var="i" value="${i+1}" />
											</c:when>
											<c:otherwise>
											${tag.name}
										</c:otherwise>
										</c:choose>
									</c:forEach>
							</span></li>
							<li class="li" id="comments"><span class="commentsdec"><spring:message
										code="label.comments" />(${fn:length(newsVo.comments)})</span></li>
							<li class="li"><a
								href='<c:url value="/EditNewsPage/editNews/${newsVo.news.id}"></c:url>'><spring:message
										code="label.edit" /></a></li>
							<li class="li"><input type="checkbox" name="newsIdToDelete"
								value="${newsVo.news.id}"></li>
						</ul>
					</div>
				</div>
			</c:forEach>
			<div id="delete">
				<input type="submit"
					value="<spring:message code="label.button.delete" />"
					class="deletebutton" />
			</div>
	</c:if>
	</form>
</body>
</html>