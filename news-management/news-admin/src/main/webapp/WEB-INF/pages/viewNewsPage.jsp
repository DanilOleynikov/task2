<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${newsVo.news.title}</title>
</head>
<body>
	<div class="news">
		<div id="news-title">${newsVo.news.title}</div>
		<div id="news-author">by(${newsVo.author.name})</div>
		<div id="news-date">${newsVo.news.modificationDate}</div>
		<div id="news-fulltext">${newsVo.news.fullText}</div>

	</div>
	<c:forEach var="comment" items="${newsVo.comments}">
		<c:if test="${comment.id ne 0}">
			<div id="comment-block">
				<span style="text-decoration: underline";padding-top:20px;>${comment.creationTime}</span>
				<div id="comment-content">
					<div style="margin: 0 auto; text-align: right;">
						<form action="${pageContext.request.contextPath}/ViewNewsPage/deleteComment"
							method="POST">
							<input type="submit" class="delete-comment-button"></input> <input
								type="hidden" value="${comment.id}" name="commentId" /> <input
								type="hidden" value="${newsVo.news.id}" name="newsId" />
						</form>
					</div>
					${comment.text}

				</div>
			</div>
		</c:if>
	</c:forEach>

	<div id="add-comment-box">

		<form:form action="${pageContext.request.contextPath}/ViewNewsPage/addComment"
			method="POST" commandName="comment">
			<spring:message code="label.textarea.add-comment.placeholder"
				var="commentTextAreaPlaceholder" />
			<form:textarea path="text" style="width: 200px" required="required"
				oninput="isCommentInvalid(this)" oninvalid="isCommentInvalid(this)"
				placeholder="${commentTextAreaPlaceholder}" />
			<form:errors path="text" cssStyle="color: red" />

			<input type="hidden" name="newsId" value="${newsVo.news.id}" />
			<input type="submit"
				value="<spring:message code="label.button.add-comment"/>"></input>

		</form:form>
	</div>

	<c:if test="${previousNewsId!=0}">
		<a href="<c:url value="/ViewNewsPage/viewNews/${previousNewsId}"></c:url>">Previous</a>
	</c:if>
	<c:if test="${nextNewsId!=0}">
		<a href="<c:url value="/ViewNewsPage/viewNews/${nextNewsId}"></c:url>">Next</a>
	</c:if>
</body>
</html>