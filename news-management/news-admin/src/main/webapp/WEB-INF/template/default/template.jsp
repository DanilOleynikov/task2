<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.11.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.11.2.custom.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/ui.dropdownchecklist-1.5-min.js" />"></script>
	
<script type="text/javascript"
	src="<c:url value="/resources/js/Validation.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/Utils.js" />"></script>
	
<link
	href="<c:url value="/resources/css/ui.dropdownchecklist.themeroller.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/css/jquery-ui-1.11.2.custom.css" />"
	rel="stylesheet">
	
<script type="text/javascript">
 var validationMessages = { <spring:message code="validation.messages" /> } ;
 var tagDropDownPlaceHolder = {<spring:message code="label.select-tag" />};
 var authorNameValidationMessages = {<spring:message code="validation.author-name.messages" />};
 var tagNameValidationMessages = {<spring:message code="validation.tag-name.messages" />};
 var commentValidationMessages = {<spring:message code="validation.comments.messages" />};
 </script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

	<div id="container">
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>

		<div id="left-menu">
			<tiles:insertAttribute name="left-menu" />
		</div>


		<div id="content">
			<tiles:insertAttribute name="body" />
		</div>


		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>