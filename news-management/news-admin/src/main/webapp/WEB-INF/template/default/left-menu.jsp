<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<sec:authorize ifAllGranted="ROLE_ADMIN">

	<div id='cssmenu'>
		<ul>
			<li class='active'><a href="<c:url value='/NewsListPage/page/1' />"><span><spring:message
							code="label.news-list" /></span></a></li>
			<li><a href="<c:url value='/AddNewsPage/addNews' />"><span><spring:message
							code="label.add-news" /></span></a></li>
			<li><a href="<c:url value='/ManageAuthorsPage/managingOfAuthors' />"><span><spring:message
							code="label.add-update-author" /></span></a></li>
			<li class='last'><a href="<c:url value='/ManageTagsPage/managingOfTags' />"><span><spring:message
							code="label.add-update-tag" /></span></a></li>
		</ul>
	</div>
</sec:authorize>
