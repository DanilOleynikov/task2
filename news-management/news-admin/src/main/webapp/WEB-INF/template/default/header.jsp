<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h4>
	<sec:authorize ifAllGranted="ROLE_ADMIN">
		<spring:message code="label.hello" />
		<sec:authentication property="principal.username" />! 
	<span class="btn-group">
			<button class="logoutbutton">
				<a href="<c:url value="/logout" />"><spring:message code="label.logout" /></a>
			</button>
	</sec:authorize>

	<a href="?lang=en">EN</a> <a href="?lang=ru">RU</a></span>
</h4>

<div id="header-info">
	<H1>
	<spring:message code="label.news.portal" /> -
	<spring:message code="label.administration" />
	</H1>
</div>







